# RLC Filters

## Getting Started

```
git clone https://gitlab.com/woodsoft/rlc-filters.git
cd rlc-filters
./init.sh
```

This will generate the `RLC Filters` CLAP plugin and copy it to `~/.clap/`.

## Credits
The `ZVAFilter` and `WDFIdealRLCxxx` classes are modeled after classes of the same names found in
"Designing Audio Effect Plugins in C++" by [Will C. Pirkle](https://www.willpirkle.com/).

*NOTE: Only Ubuntu is supported at this time.*
