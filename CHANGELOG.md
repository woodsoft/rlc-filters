## 0.12.0 (2024-09-14)

### added (1 change)

- [feat: Add class WDFIdealRLCBSF](https://gitlab.com/woodsoft/rlc-filters/-/commit/357493b61b8b751878bcb2ad2d3c49685c2067b4) ([merge request](https://gitlab.com/woodsoft/rlc-filters/-/merge_requests/16))

## 0.11.0 (2024-09-14)

### added (1 change)

- [feat: Add class WDFIdealRLCBPF](https://gitlab.com/woodsoft/rlc-filters/-/commit/5bc49e7d1f7d311c84d31a8827fef8a0c0341148) ([merge request](https://gitlab.com/woodsoft/rlc-filters/-/merge_requests/15))

## 0.10.0 (2024-09-13)

### added (1 change)

- [feat: Add class WDFIdealRLCHPF](https://gitlab.com/woodsoft/rlc-filters/-/commit/c4f35f8b4bb3baf91675277ae17546b386595ee5) ([merge request](https://gitlab.com/woodsoft/rlc-filters/-/merge_requests/13))

## 0.9.0 (2024-09-13)

### added (1 change)

- [feat: Add class WDFIdealRLCLPF](https://gitlab.com/woodsoft/rlc-filters/-/commit/08807e391f46c2ae1f1b456df0fc1b073c38bb1c) ([merge request](https://gitlab.com/woodsoft/rlc-filters/-/merge_requests/12))

## 0.8.0 (2024-09-13)

### added (1 change)

- [feat: Add struct WDFParameters](https://gitlab.com/woodsoft/rlc-filters/-/commit/6dc77ef9731cc64c258e653a3721c0421b64f87b) ([merge request](https://gitlab.com/woodsoft/rlc-filters/-/merge_requests/11))

## 0.7.0 (2024-09-13)

### added (1 change)

- [feat: Add class WDFConstKBPF6](https://gitlab.com/woodsoft/rlc-filters/-/commit/2a13964311bc00f3e54d308ba78b5493083c88ca) ([merge request](https://gitlab.com/woodsoft/rlc-filters/-/merge_requests/10))

## 0.6.0 (2024-09-13)

### added (1 change)

- [feat: Add class WDFBesselBSF3](https://gitlab.com/woodsoft/rlc-filters/-/commit/1e9b5f0baffd7320e3ace7b819ed328de0c4d226) ([merge request](https://gitlab.com/woodsoft/rlc-filters/-/merge_requests/9))

## 0.5.0 (2024-09-13)

### added (1 change)

- [feat: Add class WDFTunableButterLPF3](https://gitlab.com/woodsoft/rlc-filters/-/commit/9fe4c6dde17979170ceaa7fc6e9e6479dc334048) ([merge request](https://gitlab.com/woodsoft/rlc-filters/-/merge_requests/8))

## 0.4.1 (2024-09-11)

### added (1 change)

- [fix: Add class WDFButterLPF3](https://gitlab.com/woodsoft/rlc-filters/-/commit/af2f938f3667f75ba4a65cc40b60bff4c1b78585) ([merge request](https://gitlab.com/woodsoft/rlc-filters/-/merge_requests/6))

## 0.4.0 (2024-09-08)

### added (1 change)

- [feat: Add WDF Ladder Filter components](https://gitlab.com/woodsoft/rlc-filters/-/commit/f211ec3180e039017b1cb0bddbc89669f4517eaa) ([merge request](https://gitlab.com/woodsoft/rlc-filters/-/merge_requests/5))

## 0.3.0 (2024-08-27)

### added (1 change)

- [feat: Implement ZVA Filter GUI](https://gitlab.com/woodsoft/rlc-filters/-/commit/1fff1eacf041b6c32b12f1e5c7142968c6df94af) ([merge request](https://gitlab.com/woodsoft/rlc-filters/-/merge_requests/4))

## 0.2.0 (2024-08-25)

### added (1 change)

- [feat: Implement ZVAFilter](https://gitlab.com/woodsoft/rlc-filters/-/commit/bf8d66d578e5c3e33cf7ff8aeefad2011a974e40) ([merge request](https://gitlab.com/woodsoft/rlc-filters/-/merge_requests/3))

## 0.1.0 (2024-07-06)

No changes.
