#include <cstdio>
#include <cstdlib>
#include <cstring>

#include <functional>
#include <vector>
#include <string>

#include <plugin.h>

struct PluginEntry {
    using create_func = std::function<const clap_plugin_t *(const clap_host_t *)>;

    PluginEntry(const clap_plugin_descriptor_t *desc, create_func &&func)
        : descriptor(desc), create(std::move(func)) {}

    const clap_plugin_descriptor_t *descriptor;
    create_func create;
};

static std::vector<PluginEntry> plugins;
static const char *plugin_path;

template <typename T>
static void addPlugin() {
    plugins.emplace_back(T::descriptor(), [](const clap_host_t *host) -> const clap_plugin_t * {
        auto plugin = new T(plugin_path, host);
        return plugin->clapPlugin();
    });
}

static const clap_plugin_factory_t plugin_factory = {
    .get_plugin_count = [] (const clap_plugin_factory *factory) -> uint32_t {
        return plugins.size();
    },

    .get_plugin_descriptor = [] (const clap_plugin_factory *factory, uint32_t index) -> const clap_plugin_descriptor_t * {
        if (index < 0 || index >= plugins.size()) {
            return nullptr;
        }

        return plugins[index].descriptor;
    },

    .create_plugin = [] (const clap_plugin_factory *factory, const clap_host_t *host, const char *plugin_id) -> const clap_plugin_t * {
        for (auto &entry : plugins) {
            if (!strcmp(entry.descriptor->id, plugin_id)) {
                return entry.create(host);
            }
        }

        return nullptr;
    }
};

extern "C" const clap_plugin_entry_t clap_entry = {
    .clap_version = CLAP_VERSION_INIT,

    .init = [] (const char *path) -> bool {
        plugin_path = path;
        addPlugin<Plugin>();
        return true;
    },

    .deinit = [] () {},

    .get_factory = [] (const char *factoryID) -> const void * {
        // Return a pointer to our pluginFactory definition.
        return strcmp(factoryID, CLAP_PLUGIN_FACTORY_ID) ? nullptr : &plugin_factory;
    },
};
