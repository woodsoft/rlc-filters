#ifndef __UTIL_H_
#define __UTIL_H_

namespace util {
    double raw2dB(double raw);
    double dB2Raw(double dB);
    double peakGainFor_Q(double Q);
    double dBPeakGainFor_Q(double Q);
    double softClipWaveShaper(double xn, double saturation);
    double sgn(double xn);
};

#endif // __UTIL_H_
