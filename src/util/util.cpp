#include <math.h>
#include <util.h>

/**
@rawTo_dB
\ingroup FX-Functions

@brief calculates dB for given input

\param raw - value to convert to dB
\return the dB value
*/
double util::raw2dB(double raw)
{
	return 20.0*log10(raw);
}

/**
@dBTo_Raw
\ingroup FX-Functions

@brief converts dB to raw value

\param dB - value to convert to raw
\return the raw value
*/
double util::dB2Raw(double dB)
{
	return pow(10.0, (dB / 20.0));
}

/**
@peakGainFor_Q
\ingroup FX-Functions

@brief calculates the peak magnitude for a given Q

\param Q - the Q value
\return the peak gain (not in dB)
*/
double util::peakGainFor_Q(double Q)
{
	// --- no resonance at or below unity
	if (Q <= 0.707) return 1.0;
	return (Q*Q) / (pow((Q*Q - 0.25), 0.5));
}

/**
@dBPeakGainFor_Q
\ingroup FX-Functions

@brief calculates the peak magnitude in dB for a given Q

\param Q - the Q value
\return the peak gain in dB
*/
double util::dBPeakGainFor_Q(double Q)
{
	return raw2dB(peakGainFor_Q(Q));
}

/**
@softClipWaveShaper
\ingroup FX-Functions

@brief calculates hyptan waveshaper
\param xn - the input value
\param saturation  - the saturation control
\return the waveshaped output value
*/
double util::softClipWaveShaper(double xn, double saturation)
{
	// --- un-normalized soft clipper from Reiss book
	return sgn(xn)*(1.0 - exp(-fabs(saturation*xn)));
}

/**
@sgn
\ingroup FX-Functions

@brief calculates sgn( ) of input
\param xn - the input value
\return -1 if xn is negative or +1 if xn is 0 or greater
*/
double util::sgn(double xn)
{
	return (xn > 0) - (xn < 0);
}
