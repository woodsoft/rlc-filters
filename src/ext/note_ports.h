#ifndef NOTE_PORTS_H
#define NOTE_PORTS_H

#include <plugin-template.h>
#include <util.h>

namespace note_ports {
    uint32_t    count(const clap_plugin_t *plugin, bool isInput);
    bool        get(const clap_plugin_t *plugin, uint32_t index, bool isInput, clap_note_port_info_t *info);
}

#endif // NOTE_PORTS_H
