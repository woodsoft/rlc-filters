#ifndef STATE_H
#define STATE_H

#include <pthread.h>
#include <clap/clap.h>

#include <params.h>

class State {
public:
    State(Params *params);
    ~State();

    bool save(const clap_ostream_t *stream);
    bool load(const clap_istream_t *stream);

    const void *clapExtension();
private:
    Params *params;

    clap_plugin_state_t state_ext;

    void initClapExtension();
};

#endif // STATE_H
