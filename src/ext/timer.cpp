#include <timer.h>
#include <plugin.h>

Timer::Timer(const clap_host_t *host) : host(host),
    timer_support((const clap_host_timer_support_t *) host->get_extension(host, CLAP_EXT_TIMER_SUPPORT)) {
    initClapExtension();
}

Timer::~Timer() {}

bool Timer::createTimer(uint32_t period_ms, clap_id *timer_id) {
    return timer_support->register_timer(host, period_ms, timer_id);
}

void Timer::destroyTimer(clap_id *timer_id) {
    timer_support->unregister_timer(host, *timer_id);
    *timer_id = 0;
}

const void *Timer::clapExtension() {
    return &timer_ext;
}

void Timer::initClapExtension() {
    timer_ext = {
        .on_timer = [] (const clap_plugin_t *cplugin, clap_id timer_id) {
            Plugin *plugin = (Plugin *) cplugin->plugin_data;
            plugin->onTimer(timer_id);
        },
    };
}
