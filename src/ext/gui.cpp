#include <gui.h>
#include <plugin.h>

static void glfwErrorCallback(int error, const char *description) {
    fprintf(stderr, "Glfw Error %d: %s\n", error, description);
}

static GUI *getGUI(const clap_plugin_t *cplugin) {
    Plugin *plugin = (Plugin *) cplugin->plugin_data;
    return plugin->getGUI();
}

GUI::GUI(Params *params) :
    params(params),
    glsl_version("#version 130") {
    fprintf(stderr, "GUI::GUI()\n");
    this->initClapExtension();
}

GUI::~GUI() {}

bool GUI::isAPISupported(const char *api, bool is_floating) {
    fprintf(stderr, "GUI::isAPISupported()\n");
    return !strcmp(api, GUI_API) && !is_floating;
}

bool GUI::getPreferredAPI(const char **api, bool *is_floating) {
    fprintf(stderr, "GUI::getPreferredAPI()\n");
    *api = GUI_API;
    *is_floating = false;
    return true;
}

bool GUI::create(const char *api, bool is_floating) {
    this->height = GUI_HEIGHT;
    this->width = GUI_WIDTH;
    this->xdisplay = (Display *) XOpenDisplay(NULL);

    glfwSetErrorCallback(glfwErrorCallback);
    if (!glfwInit()) {
        fprintf(stderr, "Error initializing GLFW.\n");
        return false;
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    this->window = glfwCreateWindow(GUI_WIDTH, GUI_HEIGHT, "Woodsoft RLC Filters", NULL, NULL);
    if (this->window == NULL) {
        fprintf(stderr, "Error creating GLFW Window.\n");
        return false;
    }
    glfwMakeContextCurrent(this->window);
    glfwSwapInterval(1);

    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO &io = ImGui::GetIO();
    ImGui_ImplOpenGL3_Init(this->glsl_version);
    ImGui_ImplGlfw_InitForOpenGL(this->window, true);
    ImGui::StyleColorsDark();

    fprintf(stderr, "GUI::create(), this->xdisplay: %p\n", this->xdisplay);
    return true;
}

void GUI::destroy() {
    fprintf(stderr, "GUI::destroy()\n");
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    this->window = nullptr;
}

bool GUI::setScale(double scale) {
    fprintf(stderr, "GUI::setScale()\n");
    this->scale = scale;
    return true;
}

bool GUI::getSize(uint32_t *width, uint32_t *height) {
    fprintf(stderr, "GUI::getSize()\n");
    *width = this->width;
    *height = this->height;
    return true;
}

bool GUI::canResize() {
    fprintf(stderr, "GUI::canResize()\n");
    return true;
}

bool GUI::getResizeHints(clap_gui_resize_hints_t *hints) {
    fprintf(stderr, "GUI::getResizeHints()\n");
    hints->can_resize_horizontally = true;
    hints->can_resize_vertically = true;
    hints->preserve_aspect_ratio = true;
    hints->aspect_ratio_width = 1;
    hints->aspect_ratio_height = 1;

    return true;
}

bool GUI::adjustSize(uint32_t *width, uint32_t *height) {
    fprintf(stderr, "GUI::adjustSize()\n");
    return this->getSize(width, height);
}

bool GUI::setSize(uint32_t width, uint32_t height) {
    fprintf(stderr, "GUI::setSize()\n");
    this->width = width;
    this->height = height;

    return true;
}

bool GUI::setParent(const clap_window_t *window) {
    fprintf(stderr, "GUI::setParent()\n");
    this->parent_window = (Window) window->x11;

    XReparentWindow(this->xdisplay, (Window) glfwGetX11Window(this->window), this->parent_window, 0, 0);
    XFlush(this->xdisplay);

    return true;
}

bool GUI::setTransient(const clap_window_t *window) {
    fprintf(stderr, "GUI::setTransient()\n");
    return false;
}

void GUI::suggestTitle(const char *title) {
    fprintf(stderr, "GUI::suggestTitle()\n");
}

bool GUI::show() {
    this->params->syncAudioToMain();
    this->newFrame();

    ImGui::Begin("Woodsoft RLC Filters", nullptr, ImGuiWindowFlags_NoDecoration);
    drawVolume();
    drawZVAFilter();
    ImGui::End();

    this->render();
    return true;
}

bool GUI::hide() {
    fprintf(stderr, "GUI::hide()\n");
    return true;
}

bool GUI::drawVolume() {
    param_data_t *volume_data = params->getParamData(P_VOLUME);
    clap_param_info_t param_info = {};
    params->getInfo(P_VOLUME, &param_info);
    if (ImGuiKnobs::Knob("Volume", &volume_data->value, param_info.min_value, param_info.max_value, 0.1f, "%.1fdb", ImGuiKnobVariant_Tick)) {
        volume_data->changed = true;
    }
    return true;
}

bool GUI::drawZVAFilter() {
    ImGui::Dummy(ImVec2(0.0f, 20.0f));
    ImGui::Separator();
    ImGui::Text("ZVA Filter");

    ZVAFilterParameters zva_filter_params = params->getZVAFilterParameters();

    // ZVA Filter Algorithm Dropdown Menu
    param_data_t *algo_data = params->getParamData(P_ZVA_FILTER_ALGO);
    clap_param_info_t algo_info = {};
    params->getInfo(P_ZVA_FILTER_ALGO, &algo_info);
    const char *filter_algos[] = {
        "LPF1", "HPF1", "APF1",
        "SVF_LP", "SVF_HP", "SVF_BP", "SVF_BS",
    };
    static int filter_algo_choice = algo_info.default_value;
    if (ImGui::Combo("ZVA Filter Algorithm", &filter_algo_choice, filter_algos, IM_ARRAYSIZE(filter_algos))) {
        zva_filter_params.filter_algorithm = (ZVAFilterAlgorithm) filter_algo_choice;
        algo_data->value = (double) filter_algo_choice;
        algo_data->changed = true;
    }

    // ZVA Filter FC Knob
    param_data_t *fc_data = params->getParamData(P_ZVA_FILTER_FC);
    clap_param_info_t fc_info = {};
    params->getInfo(P_ZVA_FILTER_FC, &fc_info);
    if (ImGuiKnobs::Knob(fc_info.name, &fc_data->value, fc_info.min_value, fc_info.max_value, 10.0f, "%.1fdB", ImGuiKnobVariant_Tick)) {
        zva_filter_params.fc = fc_data->value;
        fc_data->changed = true;
    }

    ImGui::SameLine();

    // ZVA Filter Q Knob
    param_data_t *q_data = params->getParamData(P_ZVA_FILTER_Q);
    clap_param_info_t q_info = {};
    params->getInfo(P_ZVA_FILTER_Q, &q_info);
    if (ImGuiKnobs::Knob(q_info.name, &q_data->value, q_info.min_value, q_info.max_value, 0.1f, "%.1fdB", ImGuiKnobVariant_Tick)) {
        zva_filter_params.Q = q_data->value;
        q_data->changed = true;
    }

    ImGui::SameLine();

    // ZVA Filter Output Gain dB Knob
    param_data_t *ogdb_data = params->getParamData(P_ZVA_FILTER_OUTPUT_GAIN_DB);
    clap_param_info_t ogdb_info = {};
    params->getInfo(P_ZVA_FILTER_OUTPUT_GAIN_DB, &ogdb_info);
    if (ImGuiKnobs::Knob(ogdb_info.name, &ogdb_data->value, ogdb_info.min_value, ogdb_info.max_value, 0.1f, "%.1fdB", ImGuiKnobVariant_Tick)) {
        zva_filter_params.filter_output_gain_db = ogdb_data->value;
        ogdb_data->changed = true;
    }

    // ZVA Filter Enable Gain Comp
    static bool egc = false;
    param_data_t *egc_data = params->getParamData(P_ZVA_FILTER_ENABLE_GAIN_COMP);
    ImGui::Checkbox("Enable Gain Comp", &egc);
    zva_filter_params.enable_gain_comp = egc;
    egc_data->value = (double) egc;

    // ZVA Filter Match Analog Nyquist LPF
    static bool manlpf = false;
    param_data_t *manlpf_data = params->getParamData(P_ZVA_FILTER_MATCH_ANALOG_NYQUIST_LPF);
    ImGui::Checkbox("Match Analog Nyquist LPF", &manlpf);
    zva_filter_params.match_analog_nyquist_LPF = manlpf;
    manlpf_data->value = (double) manlpf;

    // ZVA Filter Self Oscillate
    static bool so = false;
    param_data_t *so_data = params->getParamData(P_ZVA_FILTER_SELF_OSCILLATE);
    ImGui::Checkbox("Self Oscillate", &so);
    zva_filter_params.self_oscillate = so;
    so_data->value = (double) so;

    // ZVA Filter Enable NLP
    static bool enlp = false;
    param_data_t *enlp_data = params->getParamData(P_ZVA_FILTER_ENABLE_NLP);
    ImGui::Checkbox("Enable NLP", &enlp);
    zva_filter_params.enable_NLP = enlp;
    enlp_data->value = (double) enlp;

    params->setZVAFilterParameters(zva_filter_params);

    return true;
}

const void *GUI::clapExtension() {
    fprintf(stderr, "GUI::clapExtension()\n");
    return &gui_ext;
}

void GUI::newFrame() {
    glfwPollEvents();

    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    XWindowAttributes xwa;
    XGetWindowAttributes(this->xdisplay, this->parent_window, &xwa);
    Window xroot = DefaultRootWindow(this->xdisplay), xchild = 0;

    if (xroot) {
        XTranslateCoordinates(this->xdisplay, this->parent_window, xroot, 0, 0, &xwa.x, &xwa.y, &xchild);
    }

    ImGui::SetNextWindowPos(ImVec2(0, 0));
    ImGui::SetNextWindowSize(ImVec2(xwa.width, xwa.height));
}

void GUI::render() {
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

    glfwSwapBuffers(this->window);

    XReparentWindow(this->xdisplay, (Window) glfwGetX11Window(this->window), this->parent_window, 0, 0);
    XFlush(this->xdisplay);
}

void GUI::initClapExtension() {
    fprintf(stderr, "GUI::initClapExtension()\n");
    gui_ext = {
        .is_api_supported = [] (const clap_plugin_t *cplugin, const char *api, bool is_floating) -> bool {
            return getGUI(cplugin)->isAPISupported(api, is_floating);
        },

        .get_preferred_api = [] (const clap_plugin_t *cplugin, const char **api, bool *is_floating) -> bool {
            return getGUI(cplugin)->getPreferredAPI(api, is_floating);
        },

        .create = [] (const clap_plugin_t *cplugin, const char *api, bool is_floating) -> bool {
            bool result = getGUI(cplugin)->create(api, is_floating);
            ((Plugin *) cplugin->plugin_data)->startTimer();

            return result;
        },

        .destroy = [] (const clap_plugin_t *cplugin) {
            getGUI(cplugin)->destroy();
            ((Plugin *) cplugin->plugin_data)->stopTimer();
        },

        .set_scale = [] (const clap_plugin_t *cplugin, double scale) -> bool {
            return getGUI(cplugin)->setScale(scale);
        },

        .get_size = [] (const clap_plugin_t *cplugin, uint32_t *width, uint32_t *height) -> bool {
            return getGUI(cplugin)->getSize(width, height);
        },
        
        .can_resize = [] (const clap_plugin_t *cplugin) -> bool {
            return getGUI(cplugin)->canResize();
        },

        .get_resize_hints = [] (const clap_plugin_t *cplugin, clap_gui_resize_hints_t *hints) -> bool {
            return getGUI(cplugin)->getResizeHints(hints);
        },

        .adjust_size = [] (const clap_plugin_t *cplugin, uint32_t *width, uint32_t *height) -> bool {
            return getGUI(cplugin)->adjustSize(width, height);
        },

        .set_size = [] (const clap_plugin_t *cplugin, uint32_t width, uint32_t height) -> bool {
            return getGUI(cplugin)->setSize(width, height);
        },

        .set_parent = [] (const clap_plugin_t *cplugin, const clap_window_t *window) -> bool {
            return getGUI(cplugin)->setParent(window);
        },

        .set_transient = [] (const clap_plugin_t *cplugin, const clap_window_t *window) -> bool {
            return getGUI(cplugin)->setTransient(window);
        },

        .suggest_title = [] (const clap_plugin_t *cplugin, const char *title) {
            getGUI(cplugin)->suggestTitle(title);
        },

        .show = [] (const clap_plugin_t *cplugin) -> bool {
            return getGUI(cplugin)->show();
        },

        .hide = [] (const clap_plugin_t *cplugin) -> bool {
            return getGUI(cplugin)->hide();
        },
    };
}
