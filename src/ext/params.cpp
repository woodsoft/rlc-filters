#include <cstdio>
#include <cstdlib>
#include <cstring>

#include <params.h>
#include <plugin.h>

static clap_param_info_t param_info[] = {
    {
        .id             = P_VOLUME,
        .flags          = CLAP_PARAM_IS_AUTOMATABLE,
        .cookie         = nullptr,
        .name           = "Volume",
        .module         = "",
        .min_value      = -60.0f,
        .max_value      = 12.0f,
        .default_value  = -3.0f,
    },
    {
        .id             = P_ZVA_FILTER_ALGO,
        .flags          = CLAP_PARAM_IS_AUTOMATABLE,
        .cookie         = nullptr,
        .name           = "ZVA Filter Algorithm",
        .module         = "",
        .min_value      = (double) ZVAFilterAlgorithm::LPF1,
        .max_value      = (double) ZVAFilterAlgorithm::SVF_BS,
        .default_value  = (double) ZVAFilterAlgorithm::SVF_LP,
    },
    {
        .id             = P_ZVA_FILTER_FC,
        .flags          = CLAP_PARAM_IS_AUTOMATABLE,
        .cookie         = nullptr,
        .name           = "ZVA Filter FC",
        .module         = "",
        .min_value      = 20.0,
        .max_value      = 20480.0,
        .default_value  = 1000.0,
    },
    {
        .id             = P_ZVA_FILTER_Q,
        .flags          = CLAP_PARAM_IS_AUTOMATABLE,
        .cookie         = nullptr,
        .name           = "ZVA Filter Q",
        .module         = "",
        .min_value      = 0.707,
        .max_value      = 20.0,
        .default_value  = 0.707,
    },
    {
        .id             = P_ZVA_FILTER_OUTPUT_GAIN_DB,
        .flags          = CLAP_PARAM_IS_AUTOMATABLE,
        .cookie         = nullptr,
        .name           = "ZVA Filter Output Gain dB",
        .module         = "",
        .min_value      = -20.0,
        .max_value      = 20.0,
        .default_value  = 0.0,
    },
    {
        .id             = P_ZVA_FILTER_ENABLE_GAIN_COMP,
        .flags          = CLAP_PARAM_IS_AUTOMATABLE,
        .cookie         = nullptr,
        .name           = "ZVA Filter Enable Gain Comp",
        .module         = "",
        .min_value      = (double) false,
        .max_value      = (double) true,
        .default_value  = (double) false,
    },
    {
        .id             = P_ZVA_FILTER_MATCH_ANALOG_NYQUIST_LPF,
        .flags          = CLAP_PARAM_IS_AUTOMATABLE,
        .cookie         = nullptr,
        .name           = "ZVA Filter Match Analog Nyquist LPF",
        .module         = "",
        .min_value      = (double) false,
        .max_value      = (double) true,
        .default_value  = (double) false,
    },
    {
        .id             = P_ZVA_FILTER_SELF_OSCILLATE,
        .flags          = CLAP_PARAM_IS_AUTOMATABLE,
        .cookie         = nullptr,
        .name           = "ZVA Filter Self Oscillate",
        .module         = "",
        .min_value      = (double) false,
        .max_value      = (double) true,
        .default_value  = (double) false,
    },
    {
        .id             = P_ZVA_FILTER_ENABLE_NLP,
        .flags          = CLAP_PARAM_IS_AUTOMATABLE,
        .cookie         = nullptr,
        .name           = "ZVA Filter Enable NLP",
        .module         = "",
        .min_value      = (double) false,
        .max_value      = (double) true,
        .default_value  = (double) false,
    },
};

static Params *getParams(const clap_plugin_t *cplugin) {
    Plugin *plugin = (Plugin *) cplugin->plugin_data;
    return plugin->getParams();
}

Params::Params() {
    initClapExtension();
    pthread_mutex_init(&params_mutex, nullptr);

    for (uint32_t i = 0; i < P_COUNT; i++) {
        param_data_t *pdata = &param_data[i];
        clap_param_info_t *pinfo = &param_info[i];

        pdata->value = pdata->main_value = pinfo->default_value;
        pdata->changed = pdata->main_changed = false;
    }
}

Params::~Params() {
    pthread_mutex_destroy(&params_mutex);
}

uint32_t Params::count() {
    return P_COUNT;
}

bool Params::getInfo(uint32_t index, clap_param_info_t *info) {
    if (index >= P_COUNT) {
        return false;
    }

    memcpy(info, &param_info[index], sizeof(clap_param_info_t));
    return true;
}

bool Params::getValue(clap_id id, double *value) {
    if (id >= P_COUNT) return false;
    param_data_t *pdata = &param_data[id];

    pthread_mutex_lock(&params_mutex);
    *value = pdata->main_changed ? pdata->main_value : pdata->value;
    pthread_mutex_unlock(&params_mutex);
    return true;
}

bool Params::valueToText(clap_id id, double value, char *display, uint32_t size) {
    switch (id) {
    case P_VOLUME:
    case P_ZVA_FILTER_ALGO:
    case P_ZVA_FILTER_FC:
    case P_ZVA_FILTER_Q:
    case P_ZVA_FILTER_OUTPUT_GAIN_DB:
    case P_ZVA_FILTER_ENABLE_GAIN_COMP:
    case P_ZVA_FILTER_MATCH_ANALOG_NYQUIST_LPF:
    case P_ZVA_FILTER_SELF_OSCILLATE:
    case P_ZVA_FILTER_ENABLE_NLP:
        snprintf(display, size, "%f", value);
        return true;
    }

    return false;
}

bool Params::textToValue(clap_id id, const char *display, double *value) {
    switch (id) {
    case P_VOLUME:
    case P_ZVA_FILTER_ALGO:
    case P_ZVA_FILTER_FC:
    case P_ZVA_FILTER_Q:
    case P_ZVA_FILTER_OUTPUT_GAIN_DB:
    case P_ZVA_FILTER_ENABLE_GAIN_COMP:
    case P_ZVA_FILTER_MATCH_ANALOG_NYQUIST_LPF:
    case P_ZVA_FILTER_SELF_OSCILLATE:
    case P_ZVA_FILTER_ENABLE_NLP:
        *value = strtod(display, NULL);
        return true;
    }

    return false;
}

void Params::flush(const clap_input_events_t *in, const clap_output_events_t *out) {}

void Params::processEvent(const clap_event_header_t *event) {
    if (event->space_id != CLAP_CORE_EVENT_SPACE_ID) return;

    switch (event->type) {
    case CLAP_EVENT_PARAM_VALUE:
        const clap_event_param_value_t *param_value = (const clap_event_param_value_t *) event;
        uint32_t id = param_value->param_id;
        double value = param_value->value;
        param_data_t *pdata = &param_data[id];

        pthread_mutex_lock(&params_mutex);
        pdata->value = value;
        pdata->changed = true;
        pthread_mutex_unlock(&params_mutex);

        // Update params structs
        switch (id) {
        case P_ZVA_FILTER_ALGO:
            zva_filter_params.filter_algorithm = (ZVAFilterAlgorithm) ((int) value);
            break;
        case P_ZVA_FILTER_FC:
            zva_filter_params.fc = value;
            break;
        case P_ZVA_FILTER_Q:
            zva_filter_params.Q = value;
            break;
        case P_ZVA_FILTER_OUTPUT_GAIN_DB:
            zva_filter_params.filter_output_gain_db = value;
            break;
        case P_ZVA_FILTER_ENABLE_GAIN_COMP:
            zva_filter_params.enable_gain_comp = ((int) value) != 0;
            break;
        case P_ZVA_FILTER_MATCH_ANALOG_NYQUIST_LPF:
            zva_filter_params.match_analog_nyquist_LPF = ((int) value) != 0;
            break;
        case P_ZVA_FILTER_SELF_OSCILLATE:
            zva_filter_params.self_oscillate = ((int) value) != 0;
            break;
        case P_ZVA_FILTER_ENABLE_NLP:
            zva_filter_params.enable_NLP = ((int) value) != 0;
            break;
        }

        syncAudioToMain();
        break;
    }
}

void Params::syncMainToAudio(const clap_output_events_t *out) {
    pthread_mutex_lock(&params_mutex);

    for (uint32_t i = 0; i < P_COUNT; i++) {
        param_data_t *pdata = &param_data[i];
        if (pdata->main_changed) {
            pdata->value = pdata->main_value;
            pdata->main_changed = false;

            clap_event_param_value_t event = {};
            event.header.size       = sizeof(event);
            event.header.time       = 0;
            event.header.space_id   = CLAP_CORE_EVENT_SPACE_ID;
            event.header.type       = CLAP_EVENT_PARAM_VALUE;
            event.header.flags      = 0;
            event.param_id          = i;
            event.cookie            = nullptr;
            event.note_id           = -1;
            event.port_index        = -1;
            event.channel           = -1;
            event.key               = -1;
            event.value             = pdata->value;
            out->try_push(out, &event.header);
        }
    }

    pthread_mutex_unlock(&params_mutex);
}

bool Params::syncAudioToMain() {
    bool any_changed = false;
    pthread_mutex_lock(&params_mutex);

    for (uint32_t i = 0; i < P_COUNT; i++) {
        param_data_t *pdata = &param_data[i];
        if (pdata->changed) {
            pdata->main_value = pdata->value;
            pdata->changed = false;
            any_changed = true;
        }
    }

    pthread_mutex_unlock(&params_mutex);
    return any_changed;
}

bool Params::save(const clap_ostream_t *stream) {
    this->syncAudioToMain();
    int64_t bytes_to_write = sizeof(param_data_t) * param_data.size();

    pthread_mutex_lock(&params_mutex);
    int64_t bytes_written = stream->write(stream, &param_data, bytes_to_write);
    pthread_mutex_unlock(&params_mutex);

    return bytes_written == bytes_to_write;
}

bool Params::load(const clap_istream_t *stream) {
    int64_t bytes_to_read = sizeof(param_data_t) * param_data.size();

    pthread_mutex_lock(&params_mutex);
    int64_t bytes_read = stream->read(stream, &param_data, bytes_to_read);
    for (uint32_t i = 0; i < P_COUNT; i++) {
        param_data[i].main_changed = true;
    }
    pthread_mutex_unlock(&params_mutex);

    return bytes_read == bytes_to_read;
}

param_data_t *Params::getParamData(clap_id id) {
    if (id >= P_COUNT) return nullptr;

    return &param_data[id];
}

ZVAFilterParameters Params::getZVAFilterParameters() {
    return zva_filter_params;
}

void Params::setZVAFilterParameters(ZVAFilterParameters params) {
    zva_filter_params = params;
}

const void *Params::clapExtension() {
    return &params_ext;
}

void Params::initClapExtension() {
    params_ext = {
        .count = [] (const clap_plugin_t *cplugin) -> uint32_t {
            return getParams(cplugin)->count();
        },

        .get_info = [] (const clap_plugin_t *cplugin, uint32_t index, clap_param_info_t *info) -> bool {
            return getParams(cplugin)->getInfo(index, info);
        },

        .get_value = [] (const clap_plugin_t *cplugin, clap_id id, double *value) -> bool {
            return getParams(cplugin)->getValue(id, value);
        },

        .value_to_text = [] (const clap_plugin_t *cplugin, clap_id id, double value, char *display, uint32_t size) -> bool {
            return getParams(cplugin)->valueToText(id, value, display, size);
        },

        .text_to_value = [] (const clap_plugin_t *cplugin, clap_id id, const char *display, double *value) -> bool {
            return getParams(cplugin)->textToValue(id, display, value);
        },

        .flush = [] (const clap_plugin_t *cplugin, const clap_input_events_t *in, const clap_output_events_t *out) {
            // First sync parameters and process new events
            Plugin *plugin = (Plugin *) cplugin->plugin_data;
            plugin->getParams()->syncMainToAudio(out);

            for (uint32_t i = 0; i < in->size(in); i++) {
                plugin->processEvent(in->get(in, i));
            }

            getParams(cplugin)->flush(in, out);
        },
    };
}
