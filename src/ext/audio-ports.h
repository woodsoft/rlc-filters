#ifndef AUDIO_PORTS_H
#define AUDIO_PORTS_H

#include <clap/clap.h>

class AudioPorts {
public:
    AudioPorts();
    ~AudioPorts();

    uint32_t count(bool is_input);
    bool get(uint32_t index, bool is_input, clap_audio_port_info_t *info);

    const void *clapExtension();
private:
    clap_plugin_audio_ports_t audio_ports;

    void initClapExtension();
};

#endif // AUDIO_PORTS_H
