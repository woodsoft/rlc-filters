#include <state.h>
#include <plugin.h>

static State *getState(const clap_plugin_t *cplugin) {
    Plugin *plugin = (Plugin *) cplugin->plugin_data;
    return plugin->getState();
}

State::State(Params *params) : params(params) {
    this->initClapExtension();
}

State::~State() {}

bool State::save(const clap_ostream_t *stream) {
    return params->save(stream);
}

bool State::load(const clap_istream_t *stream) {
    return params->load(stream);
}

const void *State::clapExtension() {
    return &state_ext;
}

void State::initClapExtension() {
    state_ext = {
        .save = [] (const clap_plugin_t *cplugin, const clap_ostream_t *stream) {
            Plugin *plugin = (Plugin *) cplugin->plugin_data;

            return getState(cplugin)->save(stream);
        },

        .load = [] (const clap_plugin_t *cplugin, const clap_istream_t *stream) {
            Plugin *plugin = (Plugin *) cplugin->plugin_data;

            return getState(cplugin)->load(stream);
        }
    };
}
