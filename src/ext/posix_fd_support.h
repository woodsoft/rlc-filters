#ifndef POSIX_FD_SUPPORT_H
#define POSIX_FD_SUPPORT_H

#include <plugin-template.h>

namespace posix_fd_support {
    void on_fd(const clap_plugin_t *plugin, int fd, clap_posix_fd_flags_t flags);
}

#endif // POSIX_FD_SUPPORT_H
