#include <wdf-butter-lpf3.h>

WDFButterLPF3::WDFButterLPF3() {
    createWDF();
}

WDFButterLPF3::~WDFButterLPF3() {}

bool WDFButterLPF3::reset(double sample_rate) {
    // reset WDF components (flush state registers)
    series_adaptor_L1.reset(sample_rate);
    parallel_adaptor_C1.reset(sample_rate);
    series_terminated_adaptor_L2.reset(sample_rate);

    // initialize the chain of adaptors
    series_adaptor_L1.initializeAdaptorChain();
    return true;
}

bool WDFButterLPF3::canProcessAudioFrame() {
    return false;
}

double WDFButterLPF3::processAudioSample(double xn) {
    // push audio sample into series L1
    series_adaptor_L1.setInput(xn);

    // output is terminated at L2's output2
    return series_terminated_adaptor_L2.getOutput2();
}

void WDFButterLPF3::createWDF() {
    // actual component values fc = 1kHz
    double l1_value = 95.49e-3;  // 95.5 mH
    double c1_value = 0.5305e-6; // 0.53 uF
    double l2_value = 95.49e-3;  // 95.5 mH

    // set adaptor components
    series_adaptor_L1.setComponent(WDFComponent::L, l1_value, 0.0);
    parallel_adaptor_C1.setComponent(WDFComponent::C, c1_value, 0.0);
    series_terminated_adaptor_L2.setComponent(WDFComponent::L, l2_value, 0.0);

    // connect adaptors
    WDFAdaptorBase::connectAdaptors(&series_adaptor_L1, &parallel_adaptor_C1);
    WDFAdaptorBase::connectAdaptors(&parallel_adaptor_C1, &series_terminated_adaptor_L2);

    series_adaptor_L1.setSourceResistance(600.0); // Rs = 600
    series_terminated_adaptor_L2.setSourceResistance(600.0); // Rload = 600
}














































