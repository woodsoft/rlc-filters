#ifndef __WDF_PARAMETERS_H_
#define __WDF_PARAMETERS_H_

struct WDFParameters {
    WDFParameters() {}
    WDFParameters& operator=(const WDFParameters& params) {
        if (this == &params) {
            return *this;
        }

        fc = params.fc;
        Q = params.Q;
        boost_cut_dB = params.boost_cut_dB;
        frequency_warping = params.frequency_warping;
        return *this;
    }

    // --- individual parameters
    double fc = 100.0;                ///< filter fc
    double Q = 0.707;                ///< filter Q
    double boost_cut_dB = 0.0;        ///< filter boost or cut in dB
    bool frequency_warping = true;    ///< enable frequency warping
};

#endif // __WDF_PARAMETERS_H_
