#include <wdf-series-rc.h>

WDFSeriesRC::WDFSeriesRC() {}

WDFSeriesRC::WDFSeriesRC(double component_valueR, double component_valueC) :
    component_valueR(component_valueR), component_valueC(component_valueC) {
}

WDFSeriesRC::~WDFSeriesRC() {}

void WDFSeriesRC::setSampleRate(double sample_rate) {
    this->sample_rate = sample_rate;
    updateComponentResistance();
}

double WDFSeriesRC::getComponentResistance() {
    return component_resistance;
}

double WDFSeriesRC::getComponentConductance() {
    return 1.0 / component_resistance;
}

void WDFSeriesRC::updateComponentResistance() {
    RR = component_valueR;
    RC = 1.0 / (2.0 * component_valueC * sample_rate);
    component_resistance = RR + RC;
    K = RR / component_resistance;
}

void WDFSeriesRC::setComponentValueRC(double component_valueR, double component_valueC) {
    this->component_valueR = component_valueR;
    this->component_valueC = component_valueC;
    updateComponentResistance();
}

void WDFSeriesRC::setComponentValueR(double component_valueR) {
    this->component_valueR = component_valueR;
    updateComponentResistance();
}

void WDFSeriesRC::setComponentValueC(double component_valueC) {
    this->component_valueC = component_valueC;
    updateComponentResistance();
}

double WDFSeriesRC::getComponentValueR() {
    return component_valueR;
}

double WDFSeriesRC::getComponentValueC() {
    return component_valueC;
}

void WDFSeriesRC::reset(double sample_rate) {
    setSampleRate(sample_rate);
    z_registerR = 0.0;
    z_registerC = 0.0;
}

void WDFSeriesRC::setInput(double in) {
    z_registerR = in;
}

double WDFSeriesRC::getOutput() {
    double NL = z_registerR;
    double out = (NL * (1.0 - K)) + (K * z_registerC);
    z_registerC = out;

    return out;
}

double WDFSeriesRC::getOutput1() { return getOutput(); }

double WDFSeriesRC::getOutput2() { return getOutput(); }

double WDFSeriesRC::getOutput3() { return getOutput(); }

void WDFSeriesRC::setInput1(double in1) {}

void WDFSeriesRC::setInput2(double in1) {}

void WDFSeriesRC::setInput3(double in1) {}
