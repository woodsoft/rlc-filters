#include <wdf-series-terminated-adaptor.h>

WDFSeriesTerminatedAdaptor::WDFSeriesTerminatedAdaptor() {}

WDFSeriesTerminatedAdaptor::~WDFSeriesTerminatedAdaptor() {}

double WDFSeriesTerminatedAdaptor::getR2() {
    double component_resistance = 0.0;

    if (getPort3CompAdaptor()) {
        component_resistance = getPort3CompAdaptor()->getComponentResistance();
    }

    R2 = R1 + component_resistance;

    return R2;
}

void WDFSeriesTerminatedAdaptor::initialize(double R1) {
    this->R1 = R1;

    double component_resistance = 0.0;

    if (getPort3CompAdaptor()) {
        component_resistance = getPort3CompAdaptor()->getComponentResistance();
    }

    B1 = (2.0 * R1) / (R1 + component_resistance + terminal_resistance);
    B3 = (2.0 * terminal_resistance) / (R1 + component_resistance + terminal_resistance);

    if (getPort2CompAdaptor()) {
        getPort2CompAdaptor()->initialize(getR2());
    }

    R3 = component_resistance;
}

void WDFSeriesTerminatedAdaptor::setInput1(double in1) {
    this->in1 = in1;

    N2 = 0.0;
    if (getPort3CompAdaptor()) {
        N2 = getPort3CompAdaptor()->getOutput();
    }

    double N3 = in1 + N2;

    out2 = -B3 * N3;

    out1 = in1 - (B1 * N3);

    N1 = -(out1 + out2 + N3);

    if (getPort1CompAdaptor()) {
        getPort1CompAdaptor()->setInput2(out1);
    }

    if (getPort3CompAdaptor()) {
        getPort3CompAdaptor()->setInput(N1);
    }
}

void WDFSeriesTerminatedAdaptor::setInput2(double in2) {
    this->in2 = in2;
}

void WDFSeriesTerminatedAdaptor::setInput3(double in3) {
    this->in3 = in3;
}

double WDFSeriesTerminatedAdaptor::getOutput1() { return out1; }

double WDFSeriesTerminatedAdaptor::getOutput2() { return out2; }

double WDFSeriesTerminatedAdaptor::getOutput3() { return out3; }
