#ifndef _WDF_CONST_KBPF6_H_
#define _WDF_CONST_KBPF6_H_

#include <wdf-component-adaptor.h>

#include <wdf-series-adaptor.h>
#include <wdf-parallel-adaptor.h>
#include <wdf-parallel-terminated-adaptor.h>

class WDFConstKBPF6 {
public:
    WDFConstKBPF6();
    ~WDFConstKBPF6();

    /** reset members to initialized state */
    virtual bool reset(double sample_rate);

    /** return false: this object only processes samples */
    virtual bool canProcessAudioFrame();

    /** process input x(n) through the WDF ladder filter to produce return value y(n) */
    /**
    \param xn input
    \return the processed sample
    */
    virtual double processAudioSample(double xn);

    /** create the WDF structure */
    void createWDF();

protected:
    // --- six adaptors
    WDFSeriesAdaptor series_adaptor_L1C1;        ///< adaptor for L1 and C1
    WDFParallelAdaptor parallel_adaptor_L2C2;    ///< adaptor for L2 and C2

    WDFSeriesAdaptor series_adaptor_L3C3;        ///< adaptor for L3 and C3
    WDFParallelAdaptor parallel_adaptor_L4C4;    ///< adaptor for L4 and C4

    WDFSeriesAdaptor series_adaptor_L5C5;        ///< adaptor for L5 and C5
    WDFParallelTerminatedAdaptor parallel_terminated_adaptor_L6C6;///< adaptor for L6 and C6
};

#endif // _WDF_CONST_KBPF6_H_
