#ifndef __DSP_H_
#define __DSP_H_

#include <params.h>

#include <zva-filter.h>
#include <wdf.h>

class DSP {
public:
    DSP(Params *params);
    ~DSP();

    double process(double in);
    void reset(double sample_rate);

private:
    Params *params;
    ZVAFilter *zva_filter;
    double volume;
    double sample_rate;

    void updateVolume();
    void updateParams();
};

#endif // __DSP_H_
