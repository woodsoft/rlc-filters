#ifndef __WDF_PARALLEL_LC_H_
#define __WDF_PARALLEL_LC_H_

#include <wdf-component-adaptor.h>

class WDFParallelLC : public WDFComponentAdaptor {
public:
    WDFParallelLC();
    WDFParallelLC(double component_valueL, double component_valueC);
    virtual ~WDFParallelLC();

    /** set sample rate and update component */
    void setSampleRate(double sample_rate);

    /** get component's value as a resistance */
    virtual double getComponentResistance();

    /** get component's value as a conducatance (or admittance) */
    virtual double getComponentConductance();

    /** change the resistance of component; see FX book for details */
    virtual void updateComponentResistance();

    /** set both LC components at once */
    virtual void setComponentValueLC(double component_valueL, double component_valueC);

    /** set L component */
    virtual void setComponentValueL(double component_valueL);

    /** set C component */
    virtual void setComponentValueC(double component_valueC);

    /** get L component value */
    virtual double getComponentValueL();

    /** get C component value */
    virtual double getComponentValueC();

    /** reset the component; clear registers */
    virtual void reset(double sample_rate);

    /** set input value into component; NOTE: K is calculated here */
    virtual void setInput(double in);

    /** get output value; NOTE: output is located in -zReg_L */
    virtual double getOutput();

    /** get output1 value; only one resistor output (not used) */
    virtual double getOutput1();

    /** get output2 value; only one resistor output (not used) */
    virtual double getOutput2();

    /** get output3 value; only one resistor output (not used) */
    virtual double getOutput3();

    /** set input1 value; not used for components */
    virtual void setInput1(double in1);

    /** set input2 value; not used for components */
    virtual void setInput2(double in2);

    /** set input3 value; not used for components */
    virtual void setInput3(double in3);

protected:
    double z_registerL = 0.0; ///< storage register for L
    double z_registerC = 0.0; ///< storage register for C

    double component_valueL = 0.0; ///< component value L
    double component_valueC = 0.0; ///< component value C

    double RL = 0.0; ///< RL value
    double RC = 0.0; ///< RC value
    double component_resistance = 0.0; ///< equivalent resistance of pair of components
    double sample_rate = 0.0; ///< sample rate
};

#endif // __WDF_PARALLEL_LC_H_
