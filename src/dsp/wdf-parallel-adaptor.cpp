#include <wdf-parallel-adaptor.h>

WDFParallelAdaptor::WDFParallelAdaptor() {}

WDFParallelAdaptor::~WDFParallelAdaptor() {}

double WDFParallelAdaptor::getR2() {
    double component_conductance = 0.0;
    if (getPort3CompAdaptor()) {
        component_conductance = getPort3CompAdaptor()->getComponentConductance();
    }

    R2 = 1.0 / ((1.0 / R1) + component_conductance);

    return R2;
}

void WDFParallelAdaptor::initialize(double R1) {
    this->R1 = R1;

    double G1 = 1.0 / R1;
    double component_conductance = 0.0;
    if (getPort3CompAdaptor()) {
        component_conductance = getPort3CompAdaptor()->getComponentConductance();
    }

    A = G1 / (G1 + component_conductance);

    if (getPort2CompAdaptor()) {
        getPort2CompAdaptor()->initialize(getR2());
    }

    R3 = 1.0 / component_conductance;
}

void WDFParallelAdaptor::setInput1(double in1) {
    this->in1 = in1;

    N2 = 0.0;
    if (getPort3CompAdaptor()) {
        N2 = getPort3CompAdaptor()->getOutput();
    }

    out2 = N2 - (A * (-in1 + N2));

    if (getPort2CompAdaptor()) {
        getPort2CompAdaptor()->setInput1(out2);
    }
}

void WDFParallelAdaptor::setInput2(double in2) {
    this->in2 = in2;

    N1 = in2 - (A * -in1 + N2);

    out1 = -in1 + N2 + N1;

    if (getPort1CompAdaptor()) {
        getPort1CompAdaptor()->setInput2(out1);
    }

    if (getPort3CompAdaptor()) {
        getPort3CompAdaptor()->setInput(N1);
    }
}

void WDFParallelAdaptor::setInput3(double in3) {}

double WDFParallelAdaptor::getOutput1() { return out1; }

double WDFParallelAdaptor::getOutput2() { return out2; }

double WDFParallelAdaptor::getOutput3() { return out3; }
