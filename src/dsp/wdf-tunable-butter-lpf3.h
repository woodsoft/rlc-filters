#ifndef _WDF_TUNABLE_BUTTER_LPF3_H_
#define _WDF_TUNABLE_BUTTER_LPF3_H_

#include <wdf-component-adaptor.h>

#include <wdf-series-adaptor.h>
#include <wdf-parallel-adaptor.h>
#include <wdf-series-terminated-adaptor.h>

class WDFTunableButterLPF3 {
public:
    WDFTunableButterLPF3();
    ~WDFTunableButterLPF3();

    /** reset members to initialized state */
    virtual bool reset(double sample_rate);

    /** return false: this object only processes samples */
    virtual bool canProcessAudioFrame();

    /** process input x(n) through the WDF ladder filter to produce return value y(n) */
    /**
    \param xn input
    \return the processed sample
    */
    virtual double processAudioSample(double xn);

    /** create the filter structure; may be called more than once */
    void createWDF();

    /** parameter setter for warping */
    void setUsePostWarping(bool b);

    /** parameter setter for fc */
    void setFilterFc(double fc_Hz);

protected:
    // --- three adapters
    WDFSeriesAdaptor series_adaptor_L1;        ///< adaptor for L1
    WDFParallelAdaptor parallel_adaptor_C1;    ///< adaptor for C1
    WDFSeriesTerminatedAdaptor series_terminated_adaptor_L2;    ///< adaptor for L2

    double L1_norm = 95.493;        // 95.5 mH
    double C1_norm = 530.516e-6;    // 0.53 uF
    double L2_norm = 95.493;        // 95.5 mH

    bool use_frequency_warping = false;    ///< flag for freq warping
    double sample_rate = 1.0;            ///< stored sample rate
};

#endif // _WDF_TUNABLE_BUTTER_LPF3_H_
