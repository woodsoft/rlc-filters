#include <cmath>
#include <dsp.h>

DSP::DSP(Params *params) : params(params) {
    zva_filter = new ZVAFilter();
}

DSP::~DSP() {
    delete zva_filter;
}

double DSP::process(double in) {
    updateParams();
    double out = zva_filter->process(in) * volume;

    return out;
}

void DSP::reset(double sample_rate) {
    this->sample_rate = sample_rate;
    zva_filter->reset(sample_rate);
}

void DSP::updateVolume() {
    param_data_t *volume_data = params->getParamData(P_VOLUME);
    volume = pow(10, (volume_data->value / 20));
}

void DSP::updateParams() {
    updateVolume();
    zva_filter->setParameters(params->getZVAFilterParameters());
}
