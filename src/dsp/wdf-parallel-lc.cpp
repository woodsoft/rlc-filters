#include <wdf-parallel-lc.h>

WDFParallelLC::WDFParallelLC() {}

WDFParallelLC::WDFParallelLC(double component_valueL, double component_valueC) :
    component_valueL(component_valueL), component_valueC(component_valueC) {
}

WDFParallelLC::~WDFParallelLC() {}

void WDFParallelLC::setSampleRate(double sample_rate) {
    this->sample_rate = sample_rate;
    updateComponentResistance();
}

double WDFParallelLC::getComponentResistance() {
    return component_resistance;
}

double WDFParallelLC::getComponentConductance() {
    return 1.0 / component_resistance;
}

void WDFParallelLC::updateComponentResistance() {
    RL = 2.0 * component_valueL * sample_rate;
    RC = 1.0 / (2.0 * component_valueC * sample_rate);
    component_resistance = (RC + (1.0 / RL));
}

void WDFParallelLC::setComponentValueLC(double component_valueL, double component_valueC) {
    this->component_valueL = component_valueL;
    this->component_valueC = component_valueC;
    updateComponentResistance();
}

void WDFParallelLC::setComponentValueL(double component_valueL) {
    this->component_valueL = component_valueL;
    updateComponentResistance();
}

void WDFParallelLC::setComponentValueC(double component_valueC) {
    this->component_valueC = component_valueC;
    updateComponentResistance();
}

double WDFParallelLC::getComponentValueL() {
    return component_valueL;
}

double WDFParallelLC::getComponentValueC() {
    return component_valueC;
}

void WDFParallelLC::reset(double sample_rate) {
    setSampleRate(sample_rate);
    z_registerL = 0.0;
    z_registerC = 0.0;
}

void WDFParallelLC::setInput(double in) {
    double YL = 1.0 / RL;
    double K = ((YL * RC) - 1.0) / ((YL * RC) + 1.0);
    double N1 = K * (in - z_registerL);
    z_registerL = N1 + z_registerC;
    z_registerC = in;
}

double WDFParallelLC::getOutput() {
    return -z_registerL;
}


double WDFParallelLC::getOutput1() {
    return getOutput();
}

double WDFParallelLC::getOutput2() {
    return getOutput();
}

double WDFParallelLC::getOutput3() {
    return getOutput();
}

void WDFParallelLC::setInput1(double in1) {}

void WDFParallelLC::setInput2(double in2) {}

void WDFParallelLC::setInput3(double in3) {}
