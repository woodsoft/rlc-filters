#ifndef __WDF_SERIES_RC_H_
#define __WDF_SERIES_RC_H_

#include <wdf-component-adaptor.h>

class WDFSeriesRC : public WDFComponentAdaptor {
public:
	WDFSeriesRC();
	WDFSeriesRC(double component_valueR, double component_valueC);
	virtual ~WDFSeriesRC();

	/** set sample rate and update component */
	void setSampleRate(double sample_rate);

	/** get component's value as a resistance */
	virtual double getComponentResistance();

	/** get component's value as a conducatance (or admittance) */
	virtual double getComponentConductance();

	/** change the resistance of component; see FX book for details */
	virtual void updateComponentResistance();

	/** set both RC components at once */
	virtual void setComponentValueRC(double component_valueR, double component_valueC);

	/** set R component */
	virtual void setComponentValueR(double component_valueR);

	/** set C component */
	virtual void setComponentValueC(double component_valueC);

	/** get R component value */
	virtual double getComponentValueR();

	/** get C component value */
	virtual double getComponentValueC();

	/** reset the component; clear registers */
	virtual void reset(double sample_rate);

	/** set input value into component */
	virtual void setInput(double in);

	/** get output value; NOTE: see FX book for details */
	virtual double getOutput();

	/** get output1 value; only one resistor output (not used) */
	virtual double getOutput1();

	/** get output2 value; only one resistor output (not used) */
	virtual double getOutput2();

	/** get output3 value; only one resistor output (not used) */
	virtual double getOutput3();

	/** set input1 value; not used for components */
	virtual void setInput1(double in1);

	/** set input2 value; not used for components */
	virtual void setInput2(double in2);

	/** set input3 value; not used for components */
	virtual void setInput3(double in3);

protected:
	double z_registerR = 0.0; ///< storage register for L
	double z_registerC = 0.0; ///< storage register for C
	double K = 0.0;

	double component_valueR = 0.0;///< component value R
	double component_valueC = 0.0;///< component value C

	double RL = 0.0;	///< RL value
	double RC = 0.0;	///< RC value
	double RR = 0.0;	///< RR value

	double component_resistance = 0.0; ///< equivalent resistance of pair of components
	double sample_rate = 0.0; ///< sample rate
};

#endif // __WDF_SERIES_RC_H_
