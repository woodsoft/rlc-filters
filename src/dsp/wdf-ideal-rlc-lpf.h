#ifndef __WDF_IDEAL_RLC_LPF_H_
#define __WDF_IDEAL_RLC_LPF_H_

#include <wdf-parameters.h>

#include <wdf-series-adaptor.h>
#include <wdf-parallel-terminated-adaptor.h>

class WDFIdealRLCLPF {
public:
    WDFIdealRLCLPF();
    ~WDFIdealRLCLPF();

    /** reset members to initialized state */
    virtual bool reset(double sample_rate);

    /** return false: this object only processes samples */
    virtual bool canProcessAudioFrame();

    /** process input x(n) through the WDF Ideal RLC filter to produce return value y(n) */
    /**
    \param xn input
    \return the processed sample
    */
    virtual double processAudioSample(double xn);

    /** create the WDF structure; may be called more than once */
    void createWDF();

    /** get parameters: note use of custom structure for passing param data */
    /**
    \return WDFParameters custom data structure
    */
    WDFParameters getParameters();

    /** set parameters: note use of custom structure for passing param data */
    /**
    \param WDFParameters custom data structure
    */
    void setParameters(const WDFParameters& _wdf_parameters);

protected:
    WDFParameters wdf_parameters;    ///< object parameters

    // --- adapters
    WDFSeriesAdaptor                series_adaptor_RL;                ///< adaptor for series RL
    WDFParallelTerminatedAdaptor    parallel_terminated_adaptor_C;    ///< adaptopr for parallel C

    double sample_rate = 1.0;

};

#endif // __WDF_IDEAL_RLC_LPF_H_
