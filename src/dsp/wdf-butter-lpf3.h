#ifndef _WDF_BUTTER_LPF3_H_
#define _WDF_BUTTER_LPF3_H_

#include <wdf-component-adaptor.h>

#include <wdf-series-adaptor.h>
#include <wdf-parallel-adaptor.h>
#include <wdf-series-terminated-adaptor.h>

class WDFButterLPF3 {
public:
    WDFButterLPF3();
    ~WDFButterLPF3();

    /** reset members to initialized state */
    virtual bool reset(double sampleRate);

    /** return false: this object only processes samples */
    virtual bool canProcessAudioFrame();

    /** process input x(n) through the WDF ladder filter to produce return value y(n) */
    /**
    \param xn input
    \return the processed sample
    */
    virtual double processAudioSample(double xn);

    /** create the WDF structure for this object - may be called more than once */
    void createWDF();

protected:
    // --- three adapters
    WDFSeriesAdaptor series_adaptor_L1;            ///< adaptor for L1
    WDFParallelAdaptor parallel_adaptor_C1;        ///< adaptor for C1
    WDFSeriesTerminatedAdaptor series_terminated_adaptor_L2;    ///< adaptor for L2
};

#endif // _WDF_BUTTER_LPF3_H_
