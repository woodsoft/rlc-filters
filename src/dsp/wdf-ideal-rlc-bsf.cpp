#include <cmath>
#include <wdf-ideal-rlc-bsf.h>

WDFIdealRLCBSF::WDFIdealRLCBSF() {
    createWDF();
}

WDFIdealRLCBSF::~WDFIdealRLCBSF() {}

bool WDFIdealRLCBSF::reset(double sample_rate) {
    this->sample_rate = sample_rate;

    // reset WDF components (flush state registers)
    series_adaptor_R.reset(sample_rate);
    parallel_terminated_adaptor_LC.reset(sample_rate);

    // initialize the chain of adaptors
    series_adaptor_R.initializeAdaptorChain();
    return true;
}

bool WDFIdealRLCBSF::canProcessAudioFrame() {
    // this object only processes audio samples
    return false;
}

double WDFIdealRLCBSF::processAudioSample(double xn) {
    series_adaptor_R.setInput1(xn);

    return 0.5 * parallel_terminated_adaptor_LC.getOutput2();
}

void WDFIdealRLCBSF::createWDF() {
    // create components
    series_adaptor_R.setComponent(WDFComponent::R, 2.533e-2, 0.0);
    parallel_terminated_adaptor_LC.setComponent(WDFComponent::seriesLC, 2.533e-2, 1.0e-6);

    // connect adaptors
    WDFAdaptorBase::connectAdaptors(&series_adaptor_R, &parallel_terminated_adaptor_LC);

    series_adaptor_R.setSourceResistance(0.0);

    parallel_terminated_adaptor_LC.setOpenTerminalResistance(true);
}

void WDFIdealRLCBSF::setParameters(const WDFParameters& wdf_parameters) {
    this->wdf_parameters = wdf_parameters;
    double fc_Hz  = wdf_parameters.fc;

    if (wdf_parameters.frequency_warping) {
        double arg = (M_PI * fc_Hz) / sample_rate;
        fc_Hz *= (tan(arg) / arg);
    }

    double inductor_val = 1.0 / (1.0e-6 * pow(2.0 * M_PI * fc_Hz, 2.0));
    double resistor_val = (1.0 / wdf_parameters.Q) * pow(inductor_val / 1.0e-6, 0.5);

    series_adaptor_R.setComponentValue(resistor_val);
    parallel_terminated_adaptor_LC.setComponentValueLC(inductor_val, 1.0e-6);
    series_adaptor_R.initializeAdaptorChain();
}
