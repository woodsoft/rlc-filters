#include <wdf-resistor.h>

WDFResistor::WDFResistor() {}

WDFResistor::WDFResistor(double component_value) : component_value(component_value) {
    z_register = 0.0;
    component_value = 0.0;
    component_resistance = 0.0;
    sample_rate = 0.0;
}

WDFResistor::~WDFResistor() {}

void WDFResistor::setSampleRate(double sample_rate) {
    this->sample_rate = sample_rate;
    updateComponentResistance();
}

double WDFResistor::getComponentResistance() {
    return component_resistance;
}

double WDFResistor::getComponentConductance() {
    return 1.0 / component_resistance;
}

double WDFResistor::getComponentValue() {
    return component_value;
}

void WDFResistor::setComponentValue(double component_value) {
    this->component_value = component_value;
    updateComponentResistance();
}

void WDFResistor::updateComponentResistance() {
    component_resistance = component_value;
}

void WDFResistor::reset(double sample_rate) {
    setSampleRate(sample_rate);
    z_register = 0.0;
}

void WDFResistor::setInput(double in) {}

double WDFResistor::getOutput() {
    return 0.0;
}

double WDFResistor::getOutput1() {
    return getOutput();
}

double WDFResistor::getOutput2() {
    return getOutput();
}

double WDFResistor::getOutput3() {
    return getOutput();
}

void WDFResistor::setInput1(double in1) {}

void WDFResistor::setInput2(double in2) {}

void WDFResistor::setInput3(double in3) {}

