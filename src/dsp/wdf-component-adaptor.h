#ifndef __WDF_COMPONENT_ADAPTOR_H_
#define __WDF_COMPONENT_ADAPTOR_H_

enum class WDFComponent {
    R, L, C,
    seriesLC, seriesRL, seriesRC,
    parallelLC, parallelRL, parallelRC,
};

struct WDFComponentInfo {
	WDFComponentInfo() {}

	WDFComponentInfo(WDFComponent component_type, double value1 = 0.0, double value2 = 0.0) {
		this->component_type = component_type;

        switch (this->component_type) {
        case WDFComponent::R:
			R = value1;
            break;
        case WDFComponent::L:
			L = value1;
            break;
        case WDFComponent::C:
			C = value1;
            break;
        case WDFComponent::seriesLC:
        case WDFComponent::parallelLC:
            L = value1;
            C = value2;
            break;
        case WDFComponent::seriesRL:
        case WDFComponent::parallelRL:
            R = value1;
            L = value2;
            break;
        case WDFComponent::seriesRC:
        case WDFComponent::parallelRC:
            R = value1;
            C = value2;
            break;
        }
	}

	double R = 0.0; ///< value of R component
	double L = 0.0;	///< value of L component
	double C = 0.0;	///< value of C component
	WDFComponent component_type = WDFComponent::R; ///< selected component type
};

class WDFComponentAdaptor {
public:
    /** initialize with source resistor R1 */
    virtual void initialize(double R1) {}

    /** initialize all downstream adaptors in the chain */
    virtual void initializeAdaptorChain() {}

    /** set input value into component port  */
    virtual void setInput(double in) {}

    /** get output value from component port  */
    virtual double getOutput() { return 0.0; }

    // --- for adaptors
    /** ADAPTOR: set input port 1  */
    virtual void setInput1(double in1) = 0;

    /** ADAPTOR: set input port 2  */
    virtual void setInput2(double in2) = 0;

    /** ADAPTOR: set input port 3 */
    virtual void setInput3(double in3) = 0;

    /** ADAPTOR: get output port 1 value */
    virtual double getOutput1() = 0;

    /** ADAPTOR: get output port 2 value */
    virtual double getOutput2() = 0;

    /** ADAPTOR: get output port 3 value */
    virtual double getOutput3() = 0;

    /** reset the object with new sample rate */
    virtual void reset(double sample_rate) {}

    /** get the commponent resistance from the attached object at Port3 */
    virtual double getComponentResistance() { return 0.0; }

    /** get the commponent conductance from the attached object at Port3 */
    virtual double getComponentConductance() { return 0.0; }

    /** update the commponent resistance at Port3 */
    virtual void updateComponentResistance() {}

    /** set an individual component value (may be R, L, or C */
    virtual void setComponentValue(double comp_val) {}

    /** set LC combined values */
    virtual void setComponentValueLC(double comp_valL, double comp_valC) {}

    /** set RL combined values */
    virtual void setComponentValueRL(double comp_valR, double comp_valL) {}

    /** set RC combined values */
    virtual void setComponentValueRC(double comp_valR, double comp_valC) {}

    /** get a component value */
    virtual double getComponentValue() { return 0.0; }
};

#endif // _WDF_COMPONENT_ADAPTOR_H_
