#include <wdf-series-lc.h>

WDFSeriesLC::WDFSeriesLC() {}

WDFSeriesLC::WDFSeriesLC(double component_valueL, double component_valueC) :
    component_valueL(component_valueL), component_valueC(component_valueC) {
}

WDFSeriesLC::~WDFSeriesLC() {}

void WDFSeriesLC::setSampleRate(double sample_rate) {
    this->sample_rate = sample_rate;
}

double WDFSeriesLC::getComponentResistance() {
    return component_resistance;
}

double WDFSeriesLC::getComponentConductance() {
    return 1.0 / component_resistance;
}

void WDFSeriesLC::updateComponentResistance() {
    RL = 2.0 * component_valueL * sample_rate;
    RC = 1.0 / (2.0 * component_valueC * sample_rate);
    component_resistance = RL + (1.0 / RC);
}

void WDFSeriesLC::setComponentValueLC(double component_valueL, double component_valueC) {
    this->component_valueL = component_valueL;
    this->component_valueC = component_valueC;
    updateComponentResistance();
}

void WDFSeriesLC::setComponentValueL(double component_valueL) {
    this->component_valueL = component_valueL;
    updateComponentResistance();
}

void WDFSeriesLC::setComponentValueC(double component_valueC) {
    this->component_valueC = component_valueC;
    updateComponentResistance();
}

double WDFSeriesLC::getComponentValueL() {
    return component_valueL;
}

double WDFSeriesLC::getComponentValueC() {
    return component_valueC;
}

void WDFSeriesLC::reset(double sample_rate) {
    setSampleRate(sample_rate);
    z_registerL = 0.0;
    z_registerC = 0.0;
}

void WDFSeriesLC::setInput(double in) {
    double YC = 1.0 / RC;
    double K = (1.0 - (RL * YC)) / (1.0 + (RL * YC));
    double N1 = K * (in - z_registerL);
    z_registerL = N1 + z_registerC;
    z_registerC = in;
}

double WDFSeriesLC::getOutput() {
    return z_registerL;
}

double WDFSeriesLC::getOutput1() {
    return getOutput();
}

double WDFSeriesLC::getOutput2() {
    return getOutput();
}

double WDFSeriesLC::getOutput3() {
    return getOutput();
}

void WDFSeriesLC::setInput1(double in1) {}

void WDFSeriesLC::setInput2(double in1) {}

void WDFSeriesLC::setInput3(double in1) {}
