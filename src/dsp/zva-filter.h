#ifndef __ZVA_FILTERS_H_
#define __ZVA_FILTERS_H_

#include <util.h>

#include <zva-filter-parameters.h>

class ZVAFilter {
public:
    ZVAFilter();
    ~ZVAFilter();

    ZVAFilterParameters getParameters();
    void setParameters(ZVAFilterParameters params);
    bool reset(double sample_rate);
    double process(double xn);
    void setSampleRate(double sample_rate);
    bool calculateFilterCoeffs();

private:
    // Filter parameters
    ZVAFilterParameters parameters;
    // Array to store two z^-1 state register values
    double integrator_z[2];
    // The alpha0 filter coefficient for input scaling and delay-free loop resolution
    double alpha0;
    // The alpha filter coefficient
    double alpha;
    // The rho filter coefficient
    double rho;
    // Variable used for the Nyquist analog matching feature
    double analog_match_sigma;
    // Current sample rate
    double sample_rate;
};

#endif // __ZVA_FILTERS_H_
