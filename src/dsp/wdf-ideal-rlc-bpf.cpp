#include <cmath>

#include <wdf-ideal-rlc-bpf.h>

WDFIdealRLCBPF::WDFIdealRLCBPF() {
    createWDF();
}

WDFIdealRLCBPF::~WDFIdealRLCBPF() {}

bool WDFIdealRLCBPF::reset(double sample_rate) {
    this->sample_rate = sample_rate;

    series_adaptor_LC.reset(sample_rate);
    parallel_terminated_adaptor_R.reset(sample_rate);

    series_adaptor_LC.initializeAdaptorChain();
    return true;
}

bool WDFIdealRLCBPF::canProcessAudioFrame() {
    return false;
}

double WDFIdealRLCBPF::processAudioSample(double xn) {
    series_adaptor_LC.setInput1(xn);

    return 0.5 * parallel_terminated_adaptor_R.getOutput2();
}

void WDFIdealRLCBPF::createWDF() {
    // create components
    series_adaptor_LC.setComponent(WDFComponent::seriesLC, 2.533e-2, 1.0e-6);
    parallel_terminated_adaptor_R.setComponent(WDFComponent::R, 2.251131e2, 0.0);

    // connect adaptors
    WDFAdaptorBase::connectAdaptors(&series_adaptor_LC, &parallel_terminated_adaptor_R);

    series_adaptor_LC.setSourceResistance(0.0);
    parallel_terminated_adaptor_R.setOpenTerminalResistance(true);
}

WDFParameters WDFIdealRLCBPF::getParameters() {
    return wdf_parameters;
}

void WDFIdealRLCBPF::setParameters(const WDFParameters& wdf_parameters) {
    this->wdf_parameters = wdf_parameters;
    double fc_Hz = wdf_parameters.fc;

    if (wdf_parameters.frequency_warping) {
        double arg = (M_PI * fc_Hz) / sample_rate;
        fc_Hz *= (tan(arg) / arg);
    }

    double inductor_val = 1.0 / (1.0e-6 * pow(2.0 * M_PI * fc_Hz, 2.0));
    double resistor_val = (1.0 / wdf_parameters.Q) * (pow(inductor_val / 1.0e-6, 0.5));

    series_adaptor_LC.setComponentValueLC(inductor_val, 1.0e-6);
    parallel_terminated_adaptor_R.setComponentValue(resistor_val);

    series_adaptor_LC.initializeAdaptorChain();
}
