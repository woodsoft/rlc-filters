#include <wdf-parallel-rl.h>

WDFParallelRL::WDFParallelRL() {}

WDFParallelRL::WDFParallelRL(double component_valueR, double component_valueL) :
    component_valueR(component_valueR), component_valueL(component_valueL) {
}

WDFParallelRL::~WDFParallelRL() {}

void WDFParallelRL::setSampleRate(double sample_rate) {
    this->sample_rate = sample_rate;
    updateComponentResistance();
}

double WDFParallelRL::getComponentResistance() {
    return component_resistance;
}

double WDFParallelRL::getComponentConductance() {
    return 1.0 / component_resistance;
}

void WDFParallelRL::updateComponentResistance() {
    RR = component_valueR;
    RL = 2.0 * component_valueL * sample_rate;
    component_resistance = 1.0 / ((1.0 / RR) + (1.0 / RL));
    K = component_resistance / RR;
}

void WDFParallelRL::setComponentValueRL(double component_valueR, double component_valueL) {
    this->component_valueR = component_valueR;
    this->component_valueL = component_valueL;
    updateComponentResistance();
}

void WDFParallelRL::setComponentValueR(double component_valueR) {
    this->component_valueR = component_valueR;
    updateComponentResistance();
}

void WDFParallelRL::setComponentValueL(double component_valueL) {
    this->component_valueL = component_valueL;
    updateComponentResistance();
}

double WDFParallelRL::getComponentValueR() {
    return component_valueR;
}

double WDFParallelRL::getComponentValueL() {
    return component_valueL;
}

void WDFParallelRL::reset(double sample_rate) {
    setSampleRate(sample_rate);
    z_registerR = 0.0;
    z_registerL = 0.0;
}

void WDFParallelRL::setInput(double in) {
    z_registerR = in;
}

double WDFParallelRL::getOutput() {
    double NL = -z_registerR;
    double out = (NL * (1.0 - K)) + (K * z_registerL);
    z_registerL = out;

    return out;
}

double WDFParallelRL::getOutput1() {
    return getOutput();
}

double WDFParallelRL::getOutput2() {
    return getOutput();
}

double WDFParallelRL::getOutput3() {
    return getOutput();
}

void WDFParallelRL::setInput1(double in1) {}

void WDFParallelRL::setInput2(double in2) {}

void WDFParallelRL::setInput3(double in3) {}
