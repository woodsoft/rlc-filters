#include <cmath>

#include <wdf-tunable-butter-lpf3.h>

WDFTunableButterLPF3::WDFTunableButterLPF3() {
    createWDF();
}

WDFTunableButterLPF3::~WDFTunableButterLPF3() {}

bool WDFTunableButterLPF3::reset(double sample_rate) {
    this->sample_rate = sample_rate;

    // reset WDF components
    series_adaptor_L1.reset(sample_rate);
    parallel_adaptor_C1.reset(sample_rate);
    series_terminated_adaptor_L2.reset(sample_rate);

    // initialize the chain of adaptors
    series_adaptor_L1.initializeAdaptorChain();
    return true;
}

bool WDFTunableButterLPF3::canProcessAudioFrame() {
    // this object only processes samples
    return false;
}

double WDFTunableButterLPF3::processAudioSample(double xn) {
    // push audio sample into series L1
    series_adaptor_L1.setInput1(xn);

    // output is at terminated L2's output2
    return series_terminated_adaptor_L2.getOutput2();
}

void WDFTunableButterLPF3::createWDF() {
    // create components, initialize to normalized values fc = 1Hz
    series_adaptor_L1.setComponent(WDFComponent::L, L1_norm, 0.0);
    parallel_adaptor_C1.setComponent(WDFComponent::C, C1_norm, 0.0);
    series_terminated_adaptor_L2.setComponent(WDFComponent::L, L2_norm, 0.0);

    // connect adaptors
    WDFAdaptorBase::connectAdaptors(&series_adaptor_L1, &parallel_adaptor_C1);
    WDFAdaptorBase::connectAdaptors(&parallel_adaptor_C1, &series_terminated_adaptor_L2);

    // set source resistance
    series_adaptor_L1.setSourceResistance(600.0); // Rs = 600

    // set terminal resistance
    series_terminated_adaptor_L2.setTerminalResistance(600.0); // Rload = 600
}

void WDFTunableButterLPF3::setUsePostWarping(bool b) {
    this->use_frequency_warping = b;
}

void WDFTunableButterLPF3::setFilterFc(double fc_Hz) {
    if (use_frequency_warping) {
        double arg = (M_PI * fc_Hz) / sample_rate;
        fc_Hz = fc_Hz * (tan(arg) / arg);
    }

    series_adaptor_L1.setComponentValue(L1_norm / fc_Hz);
    parallel_adaptor_C1.setComponentValue(C1_norm / fc_Hz);
    series_terminated_adaptor_L2.setComponentValue(L2_norm / fc_Hz);
}
