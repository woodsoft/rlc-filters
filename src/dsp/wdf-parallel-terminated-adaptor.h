#ifndef _WDF_PARALLEL_TERMINATED_ADAPTOR_H_
#define _WDF_PARALLEL_TERMINATED_ADAPTOR_H_

#include <wdf-adaptor-base.h>

class WDFParallelTerminatedAdaptor : public WDFAdaptorBase {
public:
	WDFParallelTerminatedAdaptor();
	virtual ~WDFParallelTerminatedAdaptor();

	/** get the resistance at port 2;  R2 = 1.0/(sum of admittances) */
	virtual double getR2();

	/** initialize adaptor with input resistance */
	virtual void initialize(double R1);

	/** push audio input sample into incident wave input*/
	virtual void setInput1(double in1);

	/** push audio input sample into reflected wave input; this is a dead end for terminated adaptorsthis is a dead end for terminated adaptors  */
	virtual void setInput2(double in2);

	/** set input 3 always connects to component */
	virtual void setInput3(double in3);

	/** get OUT1 = reflected output pin on Port 1 */
	virtual double getOutput1();

	/** get OUT2 = incident (normal) output pin on Port 2 */
	virtual double getOutput2();

	/** get OUT3 always connects to component */
	virtual double getOutput3();

private:
	double N1 = 0.0;	///< node 1 value, internal use only
	double N2 = 0.0;	///< node 2 value, internal use only
	double A1 = 0.0;	///< A1 coefficient value
	double A3 = 0.0;	///< A3 coefficient value
};

#endif // _WDF_PARALLEL_TERMINATED_ADAPTOR_H_
