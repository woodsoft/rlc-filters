#include <wdf-capacitor.h>
#include <wdf-inductor.h>
#include <wdf-resistor.h>

#include <wdf-parallel-lc.h>
#include <wdf-parallel-rc.h>
#include <wdf-parallel-rl.h>
#include <wdf-series-lc.h>
#include <wdf-series-rc.h>
#include <wdf-series-rl.h>

#include <wdf-adaptor-base.h>

WDFAdaptorBase::WDFAdaptorBase() {}

WDFAdaptorBase::~WDFAdaptorBase() {}

void WDFAdaptorBase::setTerminalResistance(double terminal_resistance) {
    this->terminal_resistance = terminal_resistance;
}

void WDFAdaptorBase::setOpenTerminalResistance(bool open_terminal_resistance) {
    this->open_terminal_resistance = open_terminal_resistance;
    terminal_resistance = 1.0e+34; // avoid division by 0.0
}

void WDFAdaptorBase::setSourceResistance(double source_resistance) {
    this->source_resistance = source_resistance;
}

void WDFAdaptorBase::setPort1CompAdaptor(WDFComponentAdaptor *port1_comp_adaptor) {
    this->port1_comp_adaptor = port1_comp_adaptor;
}

void WDFAdaptorBase::setPort2CompAdaptor(WDFComponentAdaptor *port2_comp_adaptor) {
    this->port2_comp_adaptor = port2_comp_adaptor;
}

void WDFAdaptorBase::setPort3CompAdaptor(WDFComponentAdaptor *port3_comp_adaptor) {
    this->port3_comp_adaptor = port3_comp_adaptor;
}

void WDFAdaptorBase::reset(double sample_rate) {
    if (wdf_component) wdf_component->reset(sample_rate);
}

void WDFAdaptorBase::setComponent(WDFComponent component_type, double value1, double value2) {
    switch(component_type) {
    case WDFComponent::R:
        wdf_component = new WDFResistor;
        wdf_component->setComponentValue(value1);
        port3_comp_adaptor = wdf_component;
        break;
    case WDFComponent::L:
        wdf_component = new WDFInductor;
        wdf_component->setComponentValue(value1);
        port3_comp_adaptor = wdf_component;
        break;
    case WDFComponent::C:
        wdf_component = new WDFCapacitor;
        wdf_component->setComponentValue(value1);
        port3_comp_adaptor = wdf_component;
        break;
    case WDFComponent::seriesLC:
        wdf_component = new WDFSeriesLC;
        wdf_component->setComponentValueLC(value1, value2);
        port3_comp_adaptor = wdf_component;
        break;
    case WDFComponent::seriesRL:
        wdf_component = new WDFSeriesRL;
        wdf_component->setComponentValueRL(value1, value2);
        port3_comp_adaptor = wdf_component;
        break;
    case WDFComponent::seriesRC:
        wdf_component = new WDFSeriesRC;
        wdf_component->setComponentValueRC(value1, value2);
        port3_comp_adaptor = wdf_component;
        break;
    case WDFComponent::parallelLC:
        wdf_component = new WDFParallelLC;
        wdf_component->setComponentValueLC(value1, value2);
        port3_comp_adaptor = wdf_component;
        break;
    case WDFComponent::parallelRL:
        wdf_component = new WDFParallelRL;
        wdf_component->setComponentValueRL(value1, value2);
        port3_comp_adaptor = wdf_component;
        break;
    case WDFComponent::parallelRC:
        wdf_component = new WDFParallelRC;
        wdf_component->setComponentValueRC(value1, value2);
        port3_comp_adaptor = wdf_component;
        break;
    }
}

void WDFAdaptorBase::initializeAdaptorChain() {
    initialize(source_resistance);
}

void WDFAdaptorBase::setComponentValue(double component_value) {
    if (wdf_component) wdf_component->setComponentValue(component_value);
}

void WDFAdaptorBase::setComponentValueLC(double component_valueL, double component_valueC) {
    if (wdf_component) wdf_component->setComponentValueLC(component_valueL, component_valueC);
}

void WDFAdaptorBase::setComponentValueRL(double component_valueR, double component_valueL) {
    if (wdf_component) wdf_component->setComponentValueRL(component_valueR, component_valueL);
}

void WDFAdaptorBase::setComponentValueRC(double component_valueR, double component_valueC) {
    if (wdf_component) wdf_component->setComponentValueRC(component_valueR, component_valueC);
}

WDFComponentAdaptor *WDFAdaptorBase::getPort1CompAdaptor() {
    return port1_comp_adaptor;
}

WDFComponentAdaptor *WDFAdaptorBase::getPort2CompAdaptor() {
    return port2_comp_adaptor;
}

WDFComponentAdaptor *WDFAdaptorBase::getPort3CompAdaptor() {
    return port3_comp_adaptor;
}




























































































