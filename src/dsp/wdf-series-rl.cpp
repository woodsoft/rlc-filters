#include <wdf-series-rl.h>

WDFSeriesRL::WDFSeriesRL() {}

WDFSeriesRL::WDFSeriesRL(double component_valueR, double component_valueL) :
    component_valueR(component_valueR), component_valueL(component_valueL) {
}

WDFSeriesRL::~WDFSeriesRL() {}

void WDFSeriesRL::setSampleRate(double sample_rate) {
    this->sample_rate = sample_rate;
    updateComponentResistance();
}

double WDFSeriesRL::getComponentResistance() {
    return component_resistance;
}

double WDFSeriesRL::getComponentConductance() {
    return 1.0 / component_resistance;
}

void WDFSeriesRL::updateComponentResistance() {
    RR = component_valueR;
    RL = 2.0 * component_valueL * sample_rate;
    component_resistance = RR + RL;
    K = RR / component_resistance;
}

void WDFSeriesRL::setComponentValueRL(double component_valueR, double component_valueL) {
    this->component_valueR = component_valueR;
    this->component_valueL = component_valueL;
    updateComponentResistance();
}

void WDFSeriesRL::setComponentValueR(double component_valueR) {
    this->component_valueR = component_valueR;
    updateComponentResistance();
}

void WDFSeriesRL::setComponentValueL(double component_valueL) {
    this->component_valueL = component_valueL;
    updateComponentResistance();
}

double WDFSeriesRL::getComponentValueR() {
    return component_valueR;
}

double WDFSeriesRL::getComponentValueL() {
    return component_valueL;
}

void WDFSeriesRL::reset(double sample_rate) {
    setSampleRate(sample_rate);
    z_registerR = 0.0;
    z_registerL = 0.0;
}

void WDFSeriesRL::setInput(double in) {
    z_registerR = in;
}

double WDFSeriesRL::getOutput() {
    double NL = -z_registerR;
    double out = (NL * (1.0 - K)) - (K * z_registerL);
    z_registerL = out;

    return out;
}

double WDFSeriesRL::getOutput1() {
    return getOutput();
}

double WDFSeriesRL::getOutput2() {
    return getOutput();
}

double WDFSeriesRL::getOutput3() {
    return getOutput();
}

void WDFSeriesRL::setInput1(double in1) {}

void WDFSeriesRL::setInput2(double in2) {}

void WDFSeriesRL::setInput3(double in3) {}
