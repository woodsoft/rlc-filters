#include <wdf-bessel-bsf3.h>

WDFBesselBSF3::WDFBesselBSF3() {
    createWDF();
}

WDFBesselBSF3::~WDFBesselBSF3() {}

bool WDFBesselBSF3::reset(double sample_rate) {
    // reset WDF components (flush state registers)
    series_adaptor_L1C1.reset(sample_rate);
    parallel_adaptor_L2C2.reset(sample_rate);
    series_terminated_adaptor_L3C3.reset(sample_rate);

    // initialize the chain of adaptors
    series_adaptor_L1C1.initializeAdaptorChain();
    return true;
}

bool WDFBesselBSF3::canProcessAudioFrame() {
    // this object only processes samples
    return false;
}

double WDFBesselBSF3::processAudioSample(double xn) {
    // push audio sample into series L1
    series_adaptor_L1C1.setInput1(xn);

    // output is terminated at L3C3's output2
    return series_terminated_adaptor_L3C3.getOutput2();
}

void WDFBesselBSF3::createWDF() {
    // set component values
    // fo = 5 kHz
    // BW = 2 kHz or Q = 2.5
    series_adaptor_L1C1.setComponent(WDFComponent::parallelLC, 16.8327e-3, 0.060193e-6);
    parallel_adaptor_L2C2.setComponent(WDFComponent::seriesLC, 49.1978e-3, 0.02059e-6);
    series_terminated_adaptor_L3C3.setComponent(WDFComponent::parallelLC, 2.57755e-3, 0.393092e-6);

    // connect adaptors
    WDFAdaptorBase::connectAdaptors(&series_adaptor_L1C1, &parallel_adaptor_L2C2);
    WDFAdaptorBase::connectAdaptors(&parallel_adaptor_L2C2, &series_terminated_adaptor_L3C3);

    series_adaptor_L1C1.setSourceResistance(600.0);

    series_terminated_adaptor_L3C3.setTerminalResistance(600.0);
}
