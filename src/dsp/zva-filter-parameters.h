#ifndef __ZVA_FILTER_PARAMETERS_H_
#define __ZVA_FILTER_PARAMETERS_H_

enum class ZVAFilterAlgorithm {
    LPF1, HPF1, APF1,
    SVF_LP, SVF_HP, SVF_BP, SVF_BS
};

struct ZVAFilterParameters {
    ZVAFilterParameters () {}
    ZVAFilterParameters& operator=(const ZVAFilterParameters &params) {
        if (this == &params) return *this;

        filter_algorithm = params.filter_algorithm;
        fc = params.fc;
        Q = params.Q;
        filter_output_gain_db = params.filter_output_gain_db;
        enable_gain_comp = params.enable_gain_comp;
        match_analog_nyquist_LPF = params.match_analog_nyquist_LPF;
        self_oscillate = params.self_oscillate;
        enable_NLP = params.enable_NLP;

        return *this;
    }

    ZVAFilterAlgorithm filter_algorithm = ZVAFilterAlgorithm::SVF_LP;
    double fc = 1000.0;
    double Q = 0.707;
    double filter_output_gain_db = 0.0; // usually not used
    bool enable_gain_comp = false;
    bool match_analog_nyquist_LPF = false;
    bool self_oscillate = false;
    bool enable_NLP = false;
};

#endif // __ZVA_FILTER_PARAMETERS_H_
