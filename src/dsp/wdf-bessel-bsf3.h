#ifndef _WDF_BESSEL_BSF3_H_
#define _WDF_BESSEL_BSF3_H_

#include <wdf-component-adaptor.h>

#include <wdf-series-adaptor.h>
#include <wdf-parallel-adaptor.h>
#include <wdf-series-terminated-adaptor.h>

class WDFBesselBSF3 {
public:
    WDFBesselBSF3();
    ~WDFBesselBSF3();

    /** reset members to initialized state */
    virtual bool reset(double sample_rate);

    /** return false: this object only processes samples */
    virtual bool canProcessAudioFrame();

    /** process input x(n) through the WDF ladder filter to produce return value y(n) */
    /**
    \param xn input
    \return the processed sample
    */
    virtual double processAudioSample(double xn);

    /** create the WDF structure; may be called more than once*/
    void createWDF();

protected:
    // --- three adapters
    WDFSeriesAdaptor series_adaptor_L1C1;        ///< adaptor for L1 and C1
    WDFParallelAdaptor parallel_adaptor_L2C2;    ///< adaptor for L2 and C2
    WDFSeriesTerminatedAdaptor series_terminated_adaptor_L3C3;    ///< adaptor for L3 and C3
};

#endif // _WDF_BESSEL_BSF3_H_
