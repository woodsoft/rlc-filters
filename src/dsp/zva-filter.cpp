#include <cmath>
#include <cstdio>
#include <cstring>

#include <zva-filter.h>

#define TWO_PI (M_PI * 2)

ZVAFilter::ZVAFilter() {
    memset(&integrator_z, 0, sizeof(integrator_z[0]) * 2);
    calculateFilterCoeffs();
}

ZVAFilter::~ZVAFilter() {}

ZVAFilterParameters ZVAFilter::getParameters() {
    return parameters;
}

void ZVAFilter::setParameters(ZVAFilterParameters params) {
    parameters = params;
    calculateFilterCoeffs();
}

bool ZVAFilter::reset(double sample_rate) {
    setSampleRate(sample_rate);
    integrator_z[0] = 0.0;
    integrator_z[1] = 0.0;
    calculateFilterCoeffs();

    return true;
}

double ZVAFilter::process(double xn) {
    double output = xn;
    // fprintf(stderr, "ZVAFilter::process(%f)\n", xn);
    ZVAFilterAlgorithm filter_algorithm = parameters.filter_algorithm;

    // With gain comp enabled, we reduce the input by
    // half the gain in dB at resonant peak.
    if (parameters.enable_gain_comp) {
        double peak_dB = util::dBPeakGainFor_Q(parameters.Q);

        if (peak_dB > 0.0) {
            double half_peak_dB_gain = util::dB2Raw(-peak_dB / 2.0);
            xn *= half_peak_dB_gain;
        }
    }

    switch (filter_algorithm) {
    case ZVAFilterAlgorithm::LPF1:
    case ZVAFilterAlgorithm::HPF1:
    case ZVAFilterAlgorithm::APF1:
        double sn = integrator_z[0];

        // create vn node
        double vn = (xn - sn) * alpha;

        // form LP output
        double lpf = vn + sn;

        integrator_z[0] = vn + lpf;

        double hpf = xn - lpf;

        double apf = lpf - hpf;

        switch (filter_algorithm) {
        case ZVAFilterAlgorithm::LPF1:
            output = parameters.match_analog_nyquist_LPF ? lpf + (alpha * hpf) : lpf;
            break;
        case ZVAFilterAlgorithm::HPF1:
            output = hpf;
            break;
        case ZVAFilterAlgorithm::APF1:
            output = apf;
            break;
        default:
            // unknown filter
            output = xn;
        }

        // fprintf(stderr, "FO output: %f\n", output);
    }

    // form the HP output first
    double hpf = alpha0 * (xn - (rho * integrator_z[0]) - integrator_z[1]);

    // BPF out
    double bpf = (alpha * hpf) + integrator_z[0];
    if (parameters.enable_NLP) bpf = util::softClipWaveShaper(bpf, 1);

    // LPF out
    double lpf = (alpha * bpf) + integrator_z[1];

    // BSF out
    double bsf = hpf + lpf;

    // finite gain at Nyquist; slight error at VHF
    double sn = integrator_z[0];

    // update memory
    integrator_z[0] = (alpha * hpf) + bpf;
    integrator_z[1] = (alpha * bpf) + lpf;

    double filter_output_gain = pow(10.0, parameters.filter_output_gain_db / 20.0);

    // fprintf(stderr, "HPF: %f, BPF: %f, LPF: %f, BSF: %f, SN: %f, Filter Output Gain: %f\n", hpf, bpf, lpf, bsf, sn, filter_output_gain);

    switch (filter_algorithm) {
    case ZVAFilterAlgorithm::SVF_LP:
        if (parameters.match_analog_nyquist_LPF) lpf += analog_match_sigma * sn;
        output = filter_output_gain * lpf;
        break;
    case ZVAFilterAlgorithm::SVF_HP:
        output = filter_output_gain * hpf;
        break;
    case ZVAFilterAlgorithm::SVF_BP:
        output = filter_output_gain * bpf;
        break;
    case ZVAFilterAlgorithm::SVF_BS:
        output = filter_output_gain * bsf;
        break;
    default:
        // unknown filter
        output = filter_output_gain * lpf;
        break;
    }

    // fprintf(stderr, "output: %f\n", output);

    return output;
}

void ZVAFilter::setSampleRate(double sample_rate) {
    this->sample_rate = sample_rate;
}

bool ZVAFilter::calculateFilterCoeffs() {
    double wd = TWO_PI * parameters.fc;
    double Q = parameters.Q;
    double T = 1.0 / sample_rate;
    double wa = (2.0 / T) * tan((wd * T) / 2.0);
    double g = (wa * T) / 2.0;

    switch (parameters.filter_algorithm) {
    // 1st order filters
    case ZVAFilterAlgorithm::LPF1:
    case ZVAFilterAlgorithm::HPF1:
    case ZVAFilterAlgorithm::APF1:
        alpha = g / (1.0 + g);
        break;
    // state variable variety
    default:
        double R = parameters.self_oscillate ? 0.0 : 1.0 / (2.0 * Q);

        alpha0 = 1.0 / (1.0 + (2.0 * R * g) + (g * g));
        alpha = g;
        rho = (2.0 * R) + g;

        // sigma for analog matching version
        double f_o = (sample_rate / 2.0) / parameters.fc;
        analog_match_sigma = 1.0 / (alpha * f_o * f_o);
    }

    // fprintf(stderr, "Alpha0: %f, Alpha: %f, RHO: %f, Analog Match Sigma: %f, Sample Rate: %f\n", alpha0, alpha, rho, analog_match_sigma, sample_rate);
    return true;
}
