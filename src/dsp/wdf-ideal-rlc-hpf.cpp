#include <cmath>

#include <wdf-ideal-rlc-hpf.h>

WDFIdealRLCHPF::WDFIdealRLCHPF() {
    createWDF();
}

WDFIdealRLCHPF::~WDFIdealRLCHPF() {}

bool WDFIdealRLCHPF::reset(double sample_rate) {
    this->sample_rate = sample_rate;

    // reset WDF components (flush state registers)
    series_adaptor_RC.reset(sample_rate);
    parallel_terminated_adaptor_L.reset(sample_rate);

    // initialize the chain of adaptors
    series_adaptor_RC.initializeAdaptorChain();
    return true;
}

bool WDFIdealRLCHPF::canProcessAudioFrame() {
    // this object only processes audio samples
    return false;
}

double WDFIdealRLCHPF::processAudioSample(double xn) {
    series_adaptor_RC.setInput1(xn);

    return 0.5 * parallel_terminated_adaptor_L.getOutput2();
}

void WDFIdealRLCHPF::createWDF() {
    // --- create components, init to noramlized values fc =
    //	   initial values for fc = 1kHz Q = 0.707
    //     Holding C Constant at 1e-6
    //			   L = 2.533e-2
    //			   R = 2.251131 e2
    series_adaptor_RC.setComponent(WDFComponent::seriesRC, 2.251131e2, 1.0e-6);
    parallel_terminated_adaptor_L.setComponent(WDFComponent::L, 2.533e-2, 0.0);

    // connect adaptors
    WDFAdaptorBase::connectAdaptors(&series_adaptor_RC, &parallel_terminated_adaptor_L);

    series_adaptor_RC.setSourceResistance(0.0);

    parallel_terminated_adaptor_L.setOpenTerminalResistance(true);
}

WDFParameters WDFIdealRLCHPF::getParameters() {
    return wdf_parameters;
}

void WDFIdealRLCHPF::setParameters(const WDFParameters& wdf_parameters) {
    this->wdf_parameters = wdf_parameters;
    double fc_Hz = wdf_parameters.fc;

    if (wdf_parameters.frequency_warping) {
        double arg = (M_PI * fc_Hz) / sample_rate;
        fc_Hz *= (tan(arg) / arg);
    }

    double inductor_val = 1.0 / (1.0e-6 * pow(2.0 * M_PI * fc_Hz, 2.0));
    double resistor_val = (1.0 / wdf_parameters.Q) * (pow(inductor_val / 1.0e-6, 0.5));
}
