#include <wdf-inductor.h>

WDFInductor::WDFInductor(double component_value) : component_value(component_value) {
}

WDFInductor::WDFInductor() {}

WDFInductor::~WDFInductor() {}

void WDFInductor::setSampleRate(double sample_rate) {
    this->sample_rate = sample_rate;
    updateComponentResistance();
}

double WDFInductor::getComponentResistance() {
    return component_resistance;
}

double WDFInductor::getComponentConductance() {
    return 1.0 / component_resistance;
}

double WDFInductor::getComponentValue() {
    return component_value;
}

void WDFInductor::setComponentValue(double component_value) {
    this->component_value = component_value;
    updateComponentResistance();
}

void WDFInductor::updateComponentResistance() {
    component_resistance = 2.0 * component_value * sample_rate;
}

void WDFInductor::reset(double sample_rate) {
    setSampleRate(sample_rate);
    z_register = 0.0;
}

void WDFInductor::setInput(double in) {
    z_register = in;
}

double WDFInductor::getOutput() {
    return -z_register;
}

double WDFInductor::getOutput1() {
    return getOutput();
}

double WDFInductor::getOutput2() {
    return getOutput();
}

double WDFInductor::getOutput3() {
    return getOutput();
}

void WDFInductor::setInput1(double in1) {}

void WDFInductor::setInput2(double in2) {}

void WDFInductor::setInput3(double in3) {}
