#include <wdf-parallel-terminated-adaptor.h>

WDFParallelTerminatedAdaptor::WDFParallelTerminatedAdaptor() {}

WDFParallelTerminatedAdaptor::~WDFParallelTerminatedAdaptor() {}

double WDFParallelTerminatedAdaptor::getR2() {
    double component_conductance = 0.0;
    if (getPort3CompAdaptor()) {
        component_conductance = getPort3CompAdaptor()->getComponentConductance();
    }

    R2 = 1.0 / ((1.0 / R1) + component_conductance);

    return R2;
}

void WDFParallelTerminatedAdaptor::initialize(double R1) {
    this->R1 = R1;

    double G1 = 1.0 / R1;
    if (terminal_resistance <= 0.0) {
        terminal_resistance = 1e-15;
    }

    double G2 = 1.0 / terminal_resistance;
    double component_conductance = 0.0;
    if (getPort3CompAdaptor()) {
        component_conductance = getPort3CompAdaptor()->getComponentConductance();
    }

    A1 = (2.0 * G1) / (G1 + component_conductance + G2);
    A3 = open_terminal_resistance ? 0.0 : (2.0 * G2) / (G1 + component_conductance + G2);

    if (getPort2CompAdaptor()) {
        getPort2CompAdaptor()->initialize(getR2());
    }

    R3 = 1.0 / component_conductance;
}

void WDFParallelTerminatedAdaptor::setInput1(double in1) {
    this->in1 = in1;

    N2 = 0.0;
    if (getPort3CompAdaptor()) {
        N2 = getPort3CompAdaptor()->getOutput();
    }

    N1 = (-A1 * (-in1 + N2)) + N2 - (A3 * N2);

    out1 = -in1 + N2 + N1;

    if (getPort1CompAdaptor()) {
        getPort1CompAdaptor()->setInput2(out1);
    }

    out2 = N2 + N1;

    if (getPort3CompAdaptor()) {
        getPort3CompAdaptor()->setInput(N1);
    }
}

void WDFParallelTerminatedAdaptor::setInput2(double in2) {
    this->in2 = in2;
}

void WDFParallelTerminatedAdaptor::setInput3(double in3) {}

double WDFParallelTerminatedAdaptor::getOutput1() { return out1; }

double WDFParallelTerminatedAdaptor::getOutput2() { return out2; }

double WDFParallelTerminatedAdaptor::getOutput3() { return out3; }
