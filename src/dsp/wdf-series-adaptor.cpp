#include <wdf-series-adaptor.h>

WDFSeriesAdaptor::WDFSeriesAdaptor() {}

WDFSeriesAdaptor::~WDFSeriesAdaptor() {}

double WDFSeriesAdaptor::getR2() {
    double component_resistance = 0.0;

    if (getPort3CompAdaptor()) {
        component_resistance = getPort3CompAdaptor()->getComponentResistance();
    }

    R2 = R1 + component_resistance;

    return R2;
}

void WDFSeriesAdaptor::initialize(double R1) {
    this->R1 = R1;

    double component_resistance = 0.0;
    if (getPort3CompAdaptor()) {
        component_resistance = getPort3CompAdaptor()->getComponentResistance();
    }

    B = R1 / (R1 + component_resistance);

    if (getPort2CompAdaptor()) {
        getPort2CompAdaptor()->initialize(getR2());
    }

    R3 = component_resistance;
}

void WDFSeriesAdaptor::setInput1(double in1) {
    this->in1 = in1;

    N2 = 0.0;
    if (getPort3CompAdaptor()) {
        N2 = getPort3CompAdaptor()->getOutput();
    }

    out1 = (-in1 + N2);

    if (getPort2CompAdaptor()) {
        getPort2CompAdaptor()->setInput1(out2);
    }
}

void WDFSeriesAdaptor::setInput2(double in2) {
    this->in2 = in2;

    N1 = -(in1 - (B * (in1 + N2 + in2)) + in2);

    out1 = in1 - (B * (N2 + in2));

    if (getPort1CompAdaptor()) {
        getPort1CompAdaptor()->setInput2(out1);
    }

    if (getPort3CompAdaptor()) {
        getPort3CompAdaptor()->setInput(N1);
    }
}

void WDFSeriesAdaptor::setInput3(double in3) {}

double WDFSeriesAdaptor::getOutput1() {
    return out1;
}

double WDFSeriesAdaptor::getOutput2() {
    return out2;
}

double WDFSeriesAdaptor::getOutput3() {
    return out3;
}
