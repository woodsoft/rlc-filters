#ifndef _WDF_ADAPTOR_BASE_H_
#define _WDF_ADAPTOR_BASE_H_

#include <wdf-component-adaptor.h>

class WDFAdaptorBase : public WDFComponentAdaptor {
public:
    WDFAdaptorBase();
    virtual ~WDFAdaptorBase();

    /** set the termainal (load) resistance for terminating adaptors */
    void setTerminalResistance(double terminal_resistance);

    /** set the termainal (load) resistance as open circuit for terminating adaptors */
    void setOpenTerminalResistance(bool open_terminal_resistance);

    /** set the input (source) resistance for an input adaptor */
    void setSourceResistance(double source_resistance);

    /** set the component or connected adaptor at port 1; functions is generic and allows extending the functionality of the WDF Library */
    void setPort1CompAdaptor(WDFComponentAdaptor* port1_comp_adaptor);

    /** set the component or connected adaptor at port 2; functions is generic and allows extending the functionality of the WDF Library */
    void setPort2CompAdaptor(WDFComponentAdaptor* port2_comp_adaptor);

    /** set the component or connected adaptor at port 3; functions is generic and allows extending the functionality of the WDF Library */
    void setPort3CompAdaptor(WDFComponentAdaptor* port3_comp_adaptor);

    /** reset the connected component */
    virtual void reset(double sample_rate);

    /** creates a new WDF component and connects it to Port 3 */
    void setComponent(WDFComponent componentType, double value1, double value2);

    /** connect two adapters together upstreamAdaptor --> downstreamAdaptor */
    static void connectAdaptors(WDFAdaptorBase* upstreamAdaptor, WDFAdaptorBase* downstreamAdaptor) {
        upstreamAdaptor->setPort2CompAdaptor(downstreamAdaptor);
        downstreamAdaptor->setPort1CompAdaptor(upstreamAdaptor);
    }

    /** initialize the chain of adaptors from upstreamAdaptor --> downstreamAdaptor */
    virtual void initializeAdaptorChain();

    /** set value of single-component adaptor */
    virtual void setComponentValue(double componentValue);

    /** set LC value of mjulti-component adaptor */
    virtual void setComponentValueLC(double component_valueL, double component_valueC);

    /** set RL value of mjulti-component adaptor */
    virtual void setComponentValueRL(double component_valueR, double component_valueL);

    /** set RC value of mjulti-component adaptor */
    virtual void setComponentValueRC(double component_valueR, double component_valueC);

    /** get adaptor connected at port 1: for extended functionality; not used in WDF ladder filter library */
    WDFComponentAdaptor* getPort1CompAdaptor();

    /** get adaptor connected at port 2: for extended functionality; not used in WDF ladder filter library */
    WDFComponentAdaptor* getPort2CompAdaptor();

    /** get adaptor connected at port 3: for extended functionality; not used in WDF ladder filter library */
    WDFComponentAdaptor* getPort3CompAdaptor();

protected:
    // --- can in theory connect any port to a component OR adaptor;
    //     though this library is setup with a convention R3 = component
    WDFComponentAdaptor* port1_comp_adaptor = nullptr;    ///< componant or adaptor connected to port 1
    WDFComponentAdaptor* port2_comp_adaptor = nullptr;    ///< componant or adaptor connected to port 2
    WDFComponentAdaptor* port3_comp_adaptor = nullptr;    ///< componant or adaptor connected to port 3
    WDFComponentAdaptor* wdf_component = nullptr;        ///< WDF componant connected to port 3 (default operation)

    // --- These hold the input (R1), component (R3) and output (R2) resistances
    double R1 = 0.0; ///< input port resistance
    double R2 = 0.0; ///< output port resistance
    double R3 = 0.0; ///< component resistance

    // --- these are input variables that are stored;
    //     not used in this implementation but may be required for extended versions
    double in1 = 0.0;    ///< stored port 1 input;  not used in this implementation but may be required for extended versions
    double in2 = 0.0;    ///< stored port 2 input;  not used in this implementation but may be required for extended versions
    double in3 = 0.0;    ///< stored port 3 input;  not used in this implementation but may be required for extended versions

    // --- these are output variables that are stored;
    //     currently out2 is the only one used as it is y(n) for this library
    //     out1 and out2 are stored; not used in this implementation but may be required for extended versions
    double out1 = 0.0;    ///< stored port 1 output; not used in this implementation but may be required for extended versions
    double out2 = 0.0;    ///< stored port 2 output; it is y(n) for this library
    double out3 = 0.0;    ///< stored port 3 output; not used in this implementation but may be required for extended versions

    // --- terminal impedance
    double terminal_resistance = 600.0; ///< value of terminal (load) resistance
    bool open_terminal_resistance = false; ///< flag for open circuit load

    // --- source impedance, OK for this to be set to 0.0 for Rs = 0
    double source_resistance = 600.0; ///< source impedance; OK for this to be set to 0.0 for Rs = 0
};

#endif // _WDF_ADAPTOR_BASE_H_
