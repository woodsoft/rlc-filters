#include <wdf-const-kbpf6.h>

WDFConstKBPF6::WDFConstKBPF6() {
    createWDF();
}

WDFConstKBPF6::~WDFConstKBPF6() {}

bool WDFConstKBPF6::reset(double sample_rate) {
    // reset WDF components (flush state registers)
    series_adaptor_L1C1.reset(sample_rate);
    parallel_adaptor_L2C2.reset(sample_rate);

    series_adaptor_L3C3.reset(sample_rate);
    parallel_adaptor_L4C4.reset(sample_rate);

    series_adaptor_L5C5.reset(sample_rate);
    parallel_terminated_adaptor_L6C6.reset(sample_rate);

    // initialize the chain of adaptors
    series_adaptor_L1C1.initializeAdaptorChain();
    return true;
}

bool WDFConstKBPF6::canProcessAudioFrame() {
    // this object only processes samples
    return false;
}

double WDFConstKBPF6::processAudioSample(double xn) {
    // push audio sample into series L1
    series_adaptor_L1C1.setInput1(xn);

    // output is at terminated L6C6 output2
    return parallel_terminated_adaptor_L6C6.getOutput2();
}

void WDFConstKBPF6::createWDF() {
    // fo = 5 kHz
    // BW = 2 kHz or Q = 2.5
    series_adaptor_L1C1.setComponent(WDFComponent::seriesLC, 47.7465e-3, 0.02122e-6);
    parallel_adaptor_L2C2.setComponent(WDFComponent::parallelLC, 3.81972e-3, 0.265258e-6);

    series_adaptor_L3C3.setComponent(WDFComponent::seriesLC, 95.493e-3, 0.01061e-6);
    parallel_adaptor_L4C4.setComponent(WDFComponent::parallelLC, 3.81972e-3, 0.265258e-6);

    series_adaptor_L5C5.setComponent(WDFComponent::seriesLC, 95.493e-3, 0.01061e-6);
    parallel_terminated_adaptor_L6C6.setComponent(WDFComponent::parallelLC, 7.63944e-3, 0.132629e-6);

    // connect adaptors
    WDFAdaptorBase::connectAdaptors(&series_adaptor_L1C1, &parallel_adaptor_L2C2);
    WDFAdaptorBase::connectAdaptors(&parallel_adaptor_L2C2, &series_adaptor_L3C3);
    WDFAdaptorBase::connectAdaptors(&series_adaptor_L3C3, &parallel_adaptor_L4C4);
    WDFAdaptorBase::connectAdaptors(&parallel_adaptor_L4C4, &series_adaptor_L5C5);
    WDFAdaptorBase::connectAdaptors(&series_adaptor_L5C5, &parallel_terminated_adaptor_L6C6);

    series_adaptor_L1C1.setSourceResistance(600.0); // Ro = 600

    parallel_terminated_adaptor_L6C6.setTerminalResistance(600.0);
}






























