#include <wdf-parallel-rc.h>

WDFParallelRC::WDFParallelRC() {}

WDFParallelRC::WDFParallelRC(double component_valueR, double component_valueC) :
    component_valueR(component_valueR), component_valueC(component_valueC) {
}

WDFParallelRC::~WDFParallelRC() {}

void WDFParallelRC::setSampleRate(double sample_rate) {
    this->sample_rate = sample_rate;
    updateComponentResistance();
}

double WDFParallelRC::getComponentResistance() {
    return component_resistance;
}

double WDFParallelRC::getComponentConductance() {
    return 1.0 / component_resistance;
}

void WDFParallelRC::updateComponentResistance() {
    RR = component_valueR;
    RC = 1.0 / (2.0 * component_valueC * sample_rate);
    component_resistance = 1.0 / ((1.0 / RR) + (1.0 / RC));
    K = component_resistance / RR;
}

void WDFParallelRC::setComponentValueRC(double component_valueR, double component_valueC) {
    this->component_valueR = component_valueR;
    this->component_valueC = component_valueC;
    updateComponentResistance();
}

void WDFParallelRC::setComponentValueR(double component_valueR) {
    this->component_valueR = component_valueR;
    updateComponentResistance();
}

void WDFParallelRC::setComponentValueC(double component_valueC) {
    this->component_valueC = component_valueC;
    updateComponentResistance();
}

double WDFParallelRC::getComponentValueR() {
    return component_valueR;
}

double WDFParallelRC::getComponentValueC() {
    return component_valueC;
}

void WDFParallelRC::reset(double sample_rate) {
    setSampleRate(sample_rate);
    z_registerR = 0.0;
    z_registerC = 0.0;
}

void WDFParallelRC::setInput(double in) {
    z_registerR = in;
}

double WDFParallelRC::getOutput() {
    double NL = z_registerR;
    double out = (NL * (1.0 - K)) - (K * z_registerC);
    z_registerC = out;

    return out;
}

double WDFParallelRC::getOutput1() { return getOutput(); }

double WDFParallelRC::getOutput2() { return getOutput(); }

double WDFParallelRC::getOutput3() { return getOutput(); }

void WDFParallelRC::setInput1(double in1) {}

void WDFParallelRC::setInput2(double in2) {}

void WDFParallelRC::setInput3(double in3) {}
