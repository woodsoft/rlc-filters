#include <cmath>

#include <wdf-ideal-rlc-lpf.h>

WDFIdealRLCLPF::WDFIdealRLCLPF() {
    createWDF();
}

WDFIdealRLCLPF::~WDFIdealRLCLPF() {}

bool WDFIdealRLCLPF::reset(double sample_rate) {
    this->sample_rate = sample_rate;

    // reset WDF components (flush state registers)
    series_adaptor_RL.reset(sample_rate);
    parallel_terminated_adaptor_C.reset(sample_rate);

    // initialize the chain of adaptors
    series_adaptor_RL.initializeAdaptorChain();
    return true;
}

bool WDFIdealRLCLPF::canProcessAudioFrame() {
    // this object only processes audio samples
    return false;
}

double WDFIdealRLCLPF::processAudioSample(double xn) {
    // push audio sample into series L1
    series_adaptor_RL.setInput1(xn);

    // output is terminated at L2's output2
    // NOTE: compensation scaling by -6dB = 0.5
    // because of WDF assumption about Rs and Rload
    return 0.5 * parallel_terminated_adaptor_C.getOutput2();
}

void WDFIdealRLCLPF::createWDF() {
    // --- create components, init to noramlized values fc =
    //       initial values for fc = 1kHz Q = 0.707
    //     Holding C Constant at 1e-6
    //               L = 2.533e-2
    //               R = 2.251131 e2
    series_adaptor_RL.setComponent(WDFComponent::seriesRL, 2.251131e2, 2.533e-2);
    parallel_terminated_adaptor_C.setComponent(WDFComponent::C, 1.0e-6, 0.0);

    // connect adaptors
    WDFAdaptorBase::connectAdaptors(&series_adaptor_RL, &parallel_terminated_adaptor_C);

    series_adaptor_RL.setSourceResistance(0.0); // Rs = 600

    parallel_terminated_adaptor_C.setOpenTerminalResistance(true);
}

WDFParameters WDFIdealRLCLPF::getParameters() {
    return wdf_parameters;
}

void WDFIdealRLCLPF::setParameters(const WDFParameters& wdf_parameters) {
    this->wdf_parameters = wdf_parameters;
    double fc_Hz = wdf_parameters.fc;

    if (wdf_parameters.frequency_warping) {
        double arg = (M_PI * fc_Hz) / sample_rate;
        fc_Hz *= (tan(arg) / arg);
    }

    double inductor_val = 1.0 / (1.0e-6 * pow(2.0 * M_PI * fc_Hz, 2.0));
    double resistor_val = (1.0 / wdf_parameters.Q) * (pow(inductor_val / 1.0e-6, 0.5));

    series_adaptor_RL.setComponentValueRL(resistor_val, inductor_val);
    series_adaptor_RL.initializeAdaptorChain();
}
