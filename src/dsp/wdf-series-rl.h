#ifndef __WDF_SERIES_RL_H_
#define __WDF_SERIES_RL_H_

#include <wdf-component-adaptor.h>

class WDFSeriesRL : public WDFComponentAdaptor {
public:
	WDFSeriesRL();
	WDFSeriesRL(double component_valueR, double component_valueL);
	virtual ~WDFSeriesRL();

	/** set sample rate and update component */
	void setSampleRate(double sample_rate);

	/** get component's value as a resistance */
	virtual double getComponentResistance();

	/** get component's value as a conducatance (or admittance) */
	virtual double getComponentConductance();

	/** change the resistance of component; see FX book for details */
	virtual void updateComponentResistance();

	/** set both RL components at once */
	virtual void setComponentValueRL(double component_valueR, double component_valueL);

	/** set R component */
	virtual void setComponentValueR(double component_valueR);

	/** set L component */
	virtual void setComponentValueL(double component_valueL);

	/** get R component value */
	virtual double getComponentValueR();

	/** get L component value */
	virtual double getComponentValueL();

	/** reset the component; clear registers */
	virtual void reset(double sample_rate);

	/** set input value into component */
	virtual void setInput(double in);

	/** get output value; NOTE: see FX book for details */
	virtual double getOutput();

	/** get output1 value; only one resistor output (not used) */
	virtual double getOutput1();

	/** get output2 value; only one resistor output (not used) */
	virtual double getOutput2();

	/** get output3 value; only one resistor output (not used) */
	virtual double getOutput3();

	/** set input1 value; not used for components */
	virtual void setInput1(double in1);

	/** set input2 value; not used for components */
	virtual void setInput2(double in2);

	/** set input3 value; not used for components */
	virtual void setInput3(double in3);

protected:
	double z_registerR = 0.0; ///< storage register for R
	double z_registerL = 0.0;///< storage register for L (not used)
	double K = 0.0;

	double component_valueR = 0.0;///< component value R
	double component_valueL = 0.0;///< component value L

	double RR = 0.0; ///< RR value
	double RL = 0.0; ///< RL value

	double component_resistance = 0.0; ///< equivalent resistance of pair of componen
	double sample_rate = 0.0; ///< sample rate
};

#endif // __WDF_SERIES_RL_H_
