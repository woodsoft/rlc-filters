#ifndef __WDF_RESISTOR_H_
#define __WDF_RESISTOR_H_

#include <wdf-component-adaptor.h>

class WDFResistor : public WDFComponentAdaptor {
public:
    WDFResistor();
    WDFResistor(double component_value);
    virtual ~WDFResistor();

    /** set sample rate and update component */
    void setSampleRate(double sample_rate);

    /** get component's value as a resistance */
    virtual double getComponentResistance();

    /** get component's value as a conducatance (or admittance) */
    virtual double getComponentConductance();

    /** get the component value */
    virtual double getComponentValue();

    /** set the component value */
    virtual void setComponentValue(double component_value);

    /** change the resistance of component */
    virtual void updateComponentResistance();

    /** reset the component; clear registers */
    virtual void reset(double sample_rate);

    /** set input value into component; NOTE: resistor is dead-end energy sink so this function does nothing */
    virtual void setInput(double in);

    /** get output value; NOTE: a WDF resistor produces no reflected output */
    virtual double getOutput();

    /** get output1 value; only one resistor output (not used) */
    virtual double getOutput1();

    /** get output2 value; only one resistor output (not used) */
    virtual double getOutput2();

    /** get output3 value; only one resistor output (not used) */
    virtual double getOutput3();

    /** set input1 value; not used for components */
    virtual void setInput1(double in1);

    /** set input2 value; not used for components */
    virtual void setInput2(double in2);

    /** set input3 value; not used for components */
    virtual void setInput3(double in3);

private:
    double z_register = 0.0; //< storage register (not used with resistor)
    double component_value = 0.0; ///< component value in electronic form (ohm, farad, henry)
    double component_resistance = 0.0;///< simulated resistance
    double sample_rate = 0.0;        ///< sample rate
};

#endif // __WDF_RESISTOR_H_
