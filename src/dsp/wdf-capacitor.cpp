#include <wdf-capacitor.h>

WDFCapacitor::WDFCapacitor() {}

WDFCapacitor::WDFCapacitor(double component_value) : component_value(component_value) {}

WDFCapacitor::~WDFCapacitor() {}

void WDFCapacitor::setSampleRate(double sample_rate) {
    this->sample_rate = sample_rate;
    updateComponentResistance();
}

double WDFCapacitor::getComponentResistance() {
    return component_resistance;
}

double WDFCapacitor::getComponentConductance() {
    return 1.0 / component_resistance;
}

double WDFCapacitor::getComponentValue() {
    return component_value;
}

void WDFCapacitor::setComponentValue(double component_value) {
    this->component_value = component_value;
    updateComponentResistance();
}

void WDFCapacitor::updateComponentResistance() {
    component_resistance = 1.0 / (2.0 * component_value * sample_rate);
}

void WDFCapacitor::reset(double sample_rate) {
    setSampleRate(sample_rate);
    z_register = 0.0;
}

void WDFCapacitor::setInput(double in) {
    z_register = in;
}

double WDFCapacitor::getOutput() {
    return z_register; // z^-1
}

double WDFCapacitor::getOutput1() {
    return getOutput();
}

double WDFCapacitor::getOutput2() {
    return getOutput();
}

double WDFCapacitor::getOutput3() {
    return getOutput();
}

void WDFCapacitor::setInput1(double in1) {}

void WDFCapacitor::setInput2(double in2) {}

void WDFCapacitor::setInput3(double in3) {}
