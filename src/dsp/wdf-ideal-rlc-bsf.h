#ifndef _WDF_IDEAL_RLC_BSF_H_
#define _WDF_IDEAL_RLC_BSF_H_

#include <wdf-parameters.h>
#include <wdf-series-adaptor.h>
#include <wdf-parallel-terminated-adaptor.h>

class WDFIdealRLCBSF {
public:
    WDFIdealRLCBSF(void);
    ~WDFIdealRLCBSF(void);

    /** reset members to initialized state */
    virtual bool reset(double sample_rate);

    /** return false: this object only processes samples */
    virtual bool canProcessAudioFrame();

    /** process input x(n) through the WDF Ideal RLC filter to produce return value y(n) */
    /**
    \param xn input
    \return the processed sample
    */
    virtual double processAudioSample(double xn);

    /** create WDF structure */
    void createWDF();

    /** get parameters: note use of custom structure for passing param data */
    /**
    \return WDFParameters custom data structure
    */
    WDFParameters getParameters();

    /** set parameters: note use of custom structure for passing param data */
    /**
    \param WDFParameters custom data structure
    */
    void setParameters(const WDFParameters& wdf_parameters);

protected:
    WDFParameters wdf_parameters;    ///< object parameters

    // --- adapters
    WDFSeriesAdaptor                series_adaptor_R; ///< adaptor for series R
    WDFParallelTerminatedAdaptor    parallel_terminated_adaptor_LC; ///< adaptor for parallel LC

    double sample_rate = 1.0; ///< sample rate storage
};

#endif // _WDF_IDEAL_RLC_BSF_H_
